commondir = common/
cpp_srcs = src/main.cpp
includes = $(wildcard src/*.hpp)

all : portals 

portals : $(cpp_srcs) $(includes)
	g++ -Wall -std=c++14 -o portals -I$(commondir) -Icommon/Linux -L /usr/local/lib/ -DGL_GLEXT_PROTOTYPES $(cpp_srcs) $(commondir)Linux/MicroGlut.c $(commondir)GL_utilities.c $(commondir)LoadTGA.c -lglfw -lXt -lX11 -lGL -lm

clean :
	rm portals

