# P.O.R.T.A.L.S.
Projectivization of Rectangles to Acheive a Little Satisfaction (P.O.R.T.A.L.S.) 
is a project by me and Daniel Ericsson. We made this for a course in computer graphics
at Linköping University spring 2017. 

The "game" plays with concepts from several famous videogames, such as Portals and Antichamber
and demonstrates different graphical effects, such as: 
* Portal rendering and 4-dimensional spaces
* Oblique Frustum
* HDR rendering and tone mapping
* Normal mapping
* Glare/Bloom effect
* Soft shadow mapping
* Reflections with Fresnel effect

A walk-through of the demo can be found here: 
https://youtu.be/GZBmc3eEmeo

Implementation is made in C++14 and OpenGL 3.3. 

# Install guide lines
Make sure you got the dependencies for OpenGL, like GL, GLFW3, X11.. 

To compile just run:
make

Run the program with
./portals

We only officially support Linux with GCC.
Tried on Ubuntu 17.04 with GCC 6.3.0 and Linux Mint.


# Note for Linux users
There is a bug in GLFW version 3.2.1 and and prior that makes the 
WASD-controlls on the keyboard "jumpy" on Linux with X11. When a 
key is released the camera will keep moving for a bit. This will
be fixed in GLFW version 3.3, but when this program was written
we had to compile the beta of 3.3 from source, hence is the 
/usr/local/lib used when compiling the portals program.
It seems like this is working with Linux Mint and GLFW 3.2.1.

Also see:
https://github.com/glfw/glfw/issues/747
