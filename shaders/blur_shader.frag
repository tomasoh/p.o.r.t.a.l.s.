#version 330 core
/**
* Fragment shader two pass gaussian blur. First use this with directionX
* set to true and then false to blur i 2D.
*/

in vec2 fragmentTexCoord;
out vec4 out_Color;

uniform bool directionX;
uniform int sizeFactor;
uniform sampler2D image;

const int KERNEL_SIZE = 3;
// const float weights[KERNEL_SIZE] = float[](0.227204, 0.193829, 0.120338, 0.054364, 0.017867);

// Really a gaussian with kernel size 5, but sampled between the pixels using hardware
const float offsets[KERNEL_SIZE] = float[](0.0, 1.3846153846, 3.2307692308);
const float weights[KERNEL_SIZE] = float[](0.2270270270, 0.165902, 0.0459696);

void main(void) {
	vec2 pixelSize = 1.0/textureSize(image, 0);
	vec3 color = texture(image, fragmentTexCoord*sizeFactor).rgb * weights[0];
	
	if(directionX) {
		// Blur in x direction
		for(int i = 1; i < KERNEL_SIZE; ++i) {
			color += texture(image, fragmentTexCoord*sizeFactor + vec2(pixelSize.x*i*offsets[i], 0.0)).rgb * weights[i];
			color += texture(image, fragmentTexCoord*sizeFactor - vec2(pixelSize.x*i*offsets[i], 0.0)).rgb * weights[i];
		}
	}
	else {
		// Blur in y direction
		for(int i = 1; i < KERNEL_SIZE; ++i) {
			color += texture(image, fragmentTexCoord*sizeFactor + vec2(0.0, pixelSize.y*i*offsets[i])).rgb * weights[i];
			color += texture(image, fragmentTexCoord*sizeFactor - vec2(0.0, pixelSize.y*i*offsets[i])).rgb * weights[i];
		}
	}

	out_Color = vec4(color, 1.0);
}
