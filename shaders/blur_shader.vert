#version 330 core
/**
* Pass through vertex shader with UV-mapping. Used in post processing for
* rendering the HDR framebuffer to the default screen framebuffer.
*/

in vec3 inPosition;
in vec2 inTexCoord;

out vec2 fragmentTexCoord;

void main(void) {
	fragmentTexCoord = inTexCoord;
	gl_Position = vec4(inPosition, 1.0);
}
