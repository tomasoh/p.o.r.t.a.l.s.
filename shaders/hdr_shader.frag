#version 330 core
/**
* Fragment shader for drawing the HDR framebuffer to default screen framebuffer.
*/

in vec2 fragmentTexCoord;
out vec4 out_Color;

uniform sampler2D hdrTexture;
uniform sampler2D bloomTexture;
uniform sampler2D bloomTexture1;
uniform sampler2D bloomTexture2;
uniform bool useBloom;

const float GAMMA = 2.2;
const float EXPOSURE = 3.0;

void main(void) {
	vec3 hdrColor = texture(hdrTexture, fragmentTexCoord).rgb;
	if(useBloom) {
		hdrColor += texture(bloomTexture, fragmentTexCoord).rgb;
		hdrColor += texture(bloomTexture1, fragmentTexCoord).rgb;
		hdrColor += texture(bloomTexture2, fragmentTexCoord).rgb;
	}
	
	// Reinhard's tone mapping
// 	hdrColor /= (hdrColor + vec3(1.0));

	// Exposure mapping
	hdrColor = vec3(1.0) - exp(-hdrColor*EXPOSURE);
	
	// Gamma correction
	hdrColor = pow(hdrColor, vec3(1.0/GAMMA));
	
	out_Color = vec4(hdrColor, 1.0);
}
