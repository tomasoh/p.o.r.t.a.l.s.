#version 330 core

uniform float lightIntensity = 3.0;

in vec2 fragmentTexCoord;
uniform sampler2D textureSampler0;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 brightColor;

const float GAMMA = 2.2f;

void main(void) {
	vec3 lightColor = pow(texture(textureSampler0, fragmentTexCoord).rgb, vec3(GAMMA));
	outColor = vec4(lightColor*lightIntensity, 1.0);
	
	float brightness = dot(outColor.rgb, vec3(0.2126, 0.7152, 0.0722));
	if(brightness > 1.0)
		brightColor = vec4(outColor.rgb, 1.0);
	else
		brightColor = vec4(0.0, 0.0, 0.0, 1.0);
}
