#version 330 core

in vec3 inPosition;
in vec2 inTexCoord;

out vec2 fragmentTexCoord;

uniform mat4 modelToProj;

void main(void) {
	fragmentTexCoord = inTexCoord;
	gl_Position = modelToProj * vec4(inPosition, 1.0);
}
