#version 330

in vec2 fragmentTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 brightColor;

uniform sampler2D textureSampler0;

const float GAMMA = 2.2f;

void main(void) {
	vec3 res = pow(texture(textureSampler0, fragmentTexCoord).rgb, vec3(GAMMA));
	outColor = vec4(res, 1.0);
	float brightness = dot(res, vec3(0.2126, 0.7152, 0.0722));
	if(brightness > 1.0)
		brightColor = vec4(res, 1.0);
	else
		brightColor = vec4(0.0, 0.0, 0.0, 1.0);
}
