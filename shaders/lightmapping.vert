#version 330

in vec3 inPosition;
in vec2 inTexCoord;

out vec3 fragmentNormal;
out vec3 fragmentSurface;
out vec2 fragmentTexCoord;

uniform mat4 modelToWorld;
uniform mat4 modelToView;
uniform mat4 modelToProj;

void main(void) {
	fragmentTexCoord = inTexCoord;
	gl_Position = modelToProj * vec4(inPosition, 1.0);
}
