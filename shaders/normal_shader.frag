#version 330

in vec3 fragmentNormal;

out vec4 out_Color;

void main(void) {
	out_Color = vec4(normalize(fragmentNormal), 1.0);
}
