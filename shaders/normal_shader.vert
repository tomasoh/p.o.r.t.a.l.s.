#version 330

in vec3 inPosition;
in vec3 inNormal;

out vec3 fragmentNormal;

uniform mat4 modelToWorld;
uniform mat4 modelToView;
uniform mat4 modelToProj;


void main(void) {
	mat3 normalMatrix = mat3(modelToView);
	fragmentNormal = normalMatrix * inNormal;
	gl_Position = modelToProj * vec4(inPosition, 1.0);
}
