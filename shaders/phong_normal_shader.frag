#version 330
/**
* A shader using the Blinn-Phong shading model. Inspired by shaders from
* www.learnopengl.com, especially the "1. Advanced Lighting" one.
*
* This shader also implements a simple variant of normal mapping. The 
* mapping however only works on fragments where the bitangent is aligned
* with the Z-axis, i.e. walls.
*/

#define MAX_LIGHTS 8

in vec3 fragmentNormal;
in vec3 fragmentPosition;
in vec2 fragmentTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 brightColor;

uniform vec3 viewPosition;

// Light data
uniform int numLights;
uniform vec3 lightPositions[MAX_LIGHTS];
uniform vec3 lightColors[MAX_LIGHTS];
uniform float lightIntensities[MAX_LIGHTS];

// Material data
uniform float materialSpecularHardness = 5;
uniform float materialDiffuse = 0.8; // Diffuse reflectivity
uniform float materialSpecularity = 0.3; // Specular level
uniform float materialAmbient = 0.4; // Ambient level

// Light falloff constants
const float lightCutoffDist = 12.0f; // If fragment is further away from light than this, ignore that light contribution

const float GAMMA = 2.2f;

uniform sampler2D textureSampler0; // Regular diffuse texture
uniform sampler2D textureSampler1; // Normal map texture


// Phong shading for one point light source
vec3 phongOneLight(vec3 lightPos, vec3 normal, float intensity, vec3 lightColor) {
	vec3 lightDirection = normalize(lightPos - fragmentPosition);
	float dist = length(lightPos - fragmentPosition);
	if(dist > lightCutoffDist)
		return vec3(materialAmbient);
	float falloff = 1.0f/(dist*dist);
	
	// Diffuse part
	float diff = max(dot(normal, lightDirection), 0.0);
	diff *= materialDiffuse*intensity;
	
	// Specular part, Blinn-Phong
	vec3 viewDir = normalize(viewPosition - fragmentPosition);
	vec3 halfwayDir = normalize(lightDirection + viewDir);
	float spec = pow(max(dot(normal, halfwayDir), 0.0), materialSpecularHardness);
	spec *= materialSpecularity*intensity;
	
	// Ambient part
	float ambient = materialAmbient;
	
	return lightColor*(diff + spec + ambient)*falloff;
}


void main(void) {
	vec3 materialColor = pow(texture(textureSampler0, fragmentTexCoord).rgb, vec3(GAMMA));
	
	vec3 realNormal = normalize(fragmentNormal);
	vec3 normal = texture(textureSampler1, fragmentTexCoord).rgb;
	normal = normalize(normal*2.0 - 1.0); // Convert from [0, 1] to [-1, 1] mapping
	normal.y = -normal.y; // OpenGL uses Y-down texture mapping
	
	vec3 bitangent = vec3(0.0, 0.0, 1.0); // Cheating! Use Z-axis as bitangent
	vec3 tangent = cross(bitangent, realNormal);
	mat3 tbn = mat3(tangent, bitangent, realNormal);
	normal = normalize(tbn*normal);
	
	vec3 res = vec3(0.0f, 0.0f, 0.0f);
	// Add all point lights
	for(int i = 0; i < numLights; ++i)
		res += phongOneLight(lightPositions[i], normal, lightIntensities[i], lightColors[i]);
	res = vec3(res[0] * materialColor[0], res[1] * materialColor[1], res[2] * materialColor[2]);
	
	outColor = vec4(res, 1.0);
	float brightness = dot(res, vec3(0.2126, 0.7152, 0.0722));
	if(brightness > 1.0)
		brightColor = vec4(res, 1.0);
	else
		brightColor = vec4(0.0, 0.0, 0.0, 1.0);
}
