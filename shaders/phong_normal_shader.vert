#version 330

in vec3 inPosition;
in vec3 inNormal;
in vec2 inTexCoord;

out vec3 fragmentNormal;
out vec3 fragmentPosition;
out vec2 fragmentTexCoord;

uniform mat4 modelToWorld;
uniform mat4 modelToView;
uniform mat4 modelToProj;


void phong() {
	mat3 normalMatrix = mat3(modelToWorld);
	fragmentNormal = normalMatrix * inNormal;
	fragmentPosition = vec3(modelToWorld * vec4(inPosition, 1.0));
}


void main(void) {
	phong();
	fragmentTexCoord = inTexCoord;
	gl_Position = modelToProj * vec4(inPosition, 1.0);
}
