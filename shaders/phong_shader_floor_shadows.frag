#version 330 core
/**
* A shader using the Blinn-Phong shading model. Inspired by shaders from
* www.learnopengl.com
*
* Special features: refletcions for floor (alpha really) with fresnel effect,
* guided by specularity map.
*/

#define MAX_LIGHTS 8

in vec3 fragmentNormal;
in vec3 fragmentPosition;
in vec2 fragmentTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 brightColor;

uniform vec3 viewPosition;

// Light data
uniform int numLights;
uniform int numShadowLights;
uniform vec3 lightPositions[MAX_LIGHTS];
uniform vec3 lightColors[MAX_LIGHTS];
uniform float lightIntensities[MAX_LIGHTS];
uniform samplerCube lightDepthMap;

// Material data
uniform float materialSpecularHardness = 5;
uniform float materialDiffuse = 0.8; // Diffuse reflectivity
uniform float materialSpecularity = 0.3; // Specular level
uniform float materialAmbient = 0.4; // Ambient level

// Light falloff constants
const float lightCutoffDist = 12.0f; // If fragment is further away from light than this, ignore that light contribution

const float GAMMA = 2.2f;

uniform sampler2D textureSampler0; // Regular diffuse texture
uniform sampler2D textureSampler1; // Specular map texture

const float SHADOW_FAR = 25.0;
const float SHADOW_BIAS = 0.15;
const int SHADOW_SAMPLES = 20;

// Fresnel constants
const float IOR_AIR = 1.0;
const float IOR_MARBLE = 1.486;
const float R0 = pow((IOR_AIR - IOR_MARBLE)/(IOR_AIR + IOR_MARBLE), 2.0);

// Sample directions for soft shadows
vec3 sampleOffsetDirections[SHADOW_SAMPLES] = vec3[] 
(
	vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1), 
	vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
	vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
	vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
	vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
);


float calculateShadow(int index, vec3 lightPos) {
	vec3 fragmentToLight = fragmentPosition - lightPos;
	float currDepth = length(fragmentToLight);
	
	float shadow = 0.0;
	float viewDistance = length(viewPosition - fragmentPosition);
	float diskRadius = 0.05;
	for(int i = 0; i < SHADOW_SAMPLES; ++i) {
		float closestDepth = texture(lightDepthMap, fragmentToLight + sampleOffsetDirections[i]*diskRadius).r;
		closestDepth *= SHADOW_FAR;
		if(currDepth - SHADOW_BIAS > closestDepth)
			shadow += 1.0;
	}
	return shadow/SHADOW_SAMPLES;
}


// Phong shading for one point light source
vec3 phongOneLight(int index, vec3 lightPos, vec3 normal, float intensity, vec3 lightColor, float fresnel) {
	vec3 lightDirection = normalize(lightPos - fragmentPosition);
	float dist = length(lightPos - fragmentPosition);
	if(dist > lightCutoffDist)
		return vec3(materialAmbient);
	float falloff = 1.0f/(dist*dist);
	
	// Diffuse part
	float diff = max(dot(normal, lightDirection), 0.0);
	diff *= materialDiffuse*intensity;
	
	// Specular part, Blinn-Phong
	vec3 viewDir = normalize(viewPosition - fragmentPosition);
	vec3 halfwayDir = normalize(lightDirection + viewDir);
	float spec = pow(max(dot(normal, halfwayDir), 0.0), materialSpecularHardness);
	spec *= materialSpecularity*intensity;
	
	// Ambient part
	float ambient = materialAmbient;
	
	// Shadow calculation, if shadow light
	float shadow = 0.0;
	if(index < numShadowLights)
		shadow = calculateShadow(index, lightPos);
	
	return lightColor*((diff + fresnel*spec)*(1.0 - shadow) + ambient)*falloff;
}


void main(void) {
	vec3 materialColor = pow(texture(textureSampler0, fragmentTexCoord).rgb, vec3(GAMMA));
	float specularMapVal = texture(textureSampler1, fragmentTexCoord).r;
	vec3 normal = normalize(fragmentNormal);
	vec3 viewDir = normalize(viewPosition - fragmentPosition);
	
	// Fresnel using Schlick's approximation
	// Equation taken from Wikipedia: https://en.wikipedia.org/wiki/Schlick%27s_approximation
	float fresnel = R0 + (1 - R0)*pow(1 - dot(normal, viewDir), 5.0);
	fresnel *= specularMapVal;
	
	vec3 res = vec3(0.0f, 0.0f, 0.0f);
	// Add all point lights
	for(int i = 0; i < numLights; ++i)
		res += phongOneLight(i, lightPositions[i], normal, lightIntensities[i], lightColors[i], specularMapVal);
	res = vec3(res[0] * materialColor[0], res[1] * materialColor[1], res[2] * materialColor[2]);
	
	outColor = vec4(res, (1.0 - fresnel));
	float brightness = dot(res, vec3(0.2126, 0.7152, 0.0722));
	if(brightness > 1.0)
		brightColor = outColor;
	else
		brightColor = vec4(0.0, 0.0, 0.0, 1.0);
}
