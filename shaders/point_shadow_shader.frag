#version 330 core
/**
* Code copied from: 
* https://learnopengl.com/#!Advanced-Lighting/Shadows/Point-Shadows
*/

in vec4 fragPos;

uniform vec3 lightPos;
uniform float far_plane;

void main()
{
	// get distance between fragment and light source
	float lightDistance = length(fragPos.xyz - lightPos);

	// map to [0;1] range by dividing by far_plane
	lightDistance = lightDistance / far_plane;

	// Write this as modified depth
	gl_FragDepth = lightDistance;
}
