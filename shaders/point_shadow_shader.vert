#version 330 core
/**
* Code copied from: 
* https://learnopengl.com/#!Advanced-Lighting/Shadows/Point-Shadows
*/

in vec3 position;

uniform mat4 modelToWorld;

void main() {
	gl_Position = modelToWorld * vec4(position, 1.0);
}  
