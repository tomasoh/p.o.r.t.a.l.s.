#version 330

in vec3 inPosition;

uniform mat4 modelToProj;

void main(void) {
	gl_Position = modelToProj * vec4(inPosition, 1.0);
}
