#pragma once

#include <GLFW/glfw3.h>
#include <cmath>
#include <glm/vec3.hpp>

extern GLFWwindow* window;


const float ROTATION_SPEED = 0.00055;
const float MOVEMENT_SPEED = 2.5;



int mouseX = 300;
int mouseY = 300;
int lastX = 300;
int lastY = 300;

glm::vec3 lookAtPoint = {0.0, 0.0, 0.0};
glm::vec3 cam = {4.0, 0.0, 1.7};
float horizontalAngle;
float verticalAngle;

void move(float deltaTime) {
	glfwPollEvents();
	float runningFactor;
	if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
	if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS){
		runningFactor = 1.5;
	}
	else{
		runningFactor = 1.0;
	}
	if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		cam.x += cos(horizontalAngle) * MOVEMENT_SPEED * deltaTime * runningFactor;
		cam.y += sin(horizontalAngle) * MOVEMENT_SPEED * deltaTime * runningFactor;
	}
	if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		cam.x -= cos(horizontalAngle) * MOVEMENT_SPEED * deltaTime * runningFactor;
		cam.y -= sin(horizontalAngle) * MOVEMENT_SPEED * deltaTime * runningFactor;
	}
	if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		cam.x += cos(horizontalAngle + M_PI/2) * MOVEMENT_SPEED * deltaTime * runningFactor;
		cam.y += sin(horizontalAngle + M_PI/2) * MOVEMENT_SPEED * deltaTime * runningFactor;
	}
	if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		cam.x += cos(horizontalAngle - M_PI/2) * MOVEMENT_SPEED * deltaTime * runningFactor;
		cam.y += sin(horizontalAngle - M_PI/2) * MOVEMENT_SPEED * deltaTime * runningFactor;
	}

	float xoffset = lastX - mouseX;
	float yoffset = lastY - mouseY; // reversed since y-coordinates go from bottom to top
	lastX = mouseX;
	lastY = mouseY;

	horizontalAngle += xoffset * ROTATION_SPEED;
	verticalAngle += yoffset * ROTATION_SPEED;
	if(verticalAngle < -M_PI/2 + 0.1)
		verticalAngle = -M_PI/2 + 0.1;
	if(verticalAngle > M_PI/2 -0.1)
		verticalAngle = M_PI/2 - 0.1;
	
// 	glfwSetCursorPos(window, 300.0, 300.0);
	lookAtPoint = glm::vec3(cam.x + cos(verticalAngle)*cos(horizontalAngle), cam.y + cos(verticalAngle)*sin(horizontalAngle), cam.z + sin(verticalAngle));
}


void calculateCameraAngles() {
	horizontalAngle = atan2(lookAtPoint.y -cam.y, lookAtPoint.x -cam.x);
	verticalAngle = atan2(lookAtPoint.z -cam.z, sqrt(pow(lookAtPoint.y -cam.y, 2) + pow(lookAtPoint.x -cam.x, 2)));
}


