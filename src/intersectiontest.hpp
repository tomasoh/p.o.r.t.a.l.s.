#pragma once

#include <glm/vec3.hpp>

const float EPSILON = 1e-6;

/**
 * Calculates if the line from point lineFrom to point lineTo hits
 * the triangle defined by the three points using a variant of
 * Moller-Trombore ray intersection algorithm. It will only count as a 
 * hit if it hits the front side of the triangle, not the back side.
 */
inline bool lineTriangleHit(const glm::vec3 &p0, const glm::vec3 &p1, 
		const glm::vec3 &p2, const glm::vec3 &lineFrom, const glm::vec3 &lineTo){
	// Code adapted from Wikipedia:
	//https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
	
	glm::vec3 dir = lineTo-lineFrom;
	
	glm::vec3 e0, e1;  //Edge0, Edge1
	glm::vec3 P, Q, T;
	float det, inv_det, u, v;
	float t;

	//Find vectors for two edges sharing p0
	e0 = p1 - p0;
	e1 = p2 - p0;
	
	// Check if we are colliding with the front side of the triangle
	if(dot(dir, cross(e0, e1)) > 0.0) return false;
	
	//Begin calculating determinant - also used to calculate u parameter
	P = glm::cross(dir, e1);
	//if determinant is near zero, ray lies in plane of triangle or ray is parallel to plane of triangle
	det = glm::dot(e0, P);
	if(det > -EPSILON and det < EPSILON) return false;
	inv_det = 1.0f / det;

	//calculate distance from p0 to ray origin
	T = lineFrom - p0;

	//Calculate u parameter and test bound
	u = glm::dot(T, P) * inv_det;
	//The intersection lies outside of the triangle
	if(u < 0.0f or u > 1.0f) return false;

	//Prepare to test v parameter
	Q = glm::cross(T, e0);

	//Calculate V parameter and test bound
	v = glm::dot(dir, Q) * inv_det;
	//The intersection lies outside of the triangle
	if(v < 0.0f or u + v  > 1.0f) return false;

	t = glm::dot(e1, Q) * inv_det;
	return (t > EPSILON and t < 1.0);
}


// void testLineTriangleIntersection() {
// 	cout << "Should hit: " << endl;
// 	cout << lineTriangleHit({0.0, 0.0, 0.0}, {-1.0, 0.0, 1.0}, {1.0, 0.0, 1.0}, 
// 					{0.0, -1.0, 0.5}, {0.0, 1.0, 0.5}) << endl;
// 	cout << lineTriangleHit({0.0, 0.0, 0.0}, {-1.0, 0.0, 1.0}, {1.0, 0.0, 1.0}, 
// 					{0.2, -1.0, 0.8}, {0.2, 1.0, 0.8}) << endl;
// 	cout << lineTriangleHit({0.0, 0.0, 0.0}, {-1.0, 0.0, 1.0}, {1.0, 0.0, 1.0}, 
// 					{0.4, -1.0, 0.2}, {-0.3, 1.0, 0.8}) << endl;
// 	
// 	cout << "Should not hit: " << endl;
// 	cout << lineTriangleHit({0.0, 0.0, 0.0}, {-1.0, 0.0, 1.0}, {1.0, 0.0, 1.0}, 
// 					{0.0, 1.0, 0.5}, {0.0, -1.0, 0.5}) << endl;
// 	cout << lineTriangleHit({0.0, 0.0, 0.0}, {-1.0, 0.0, 1.0}, {1.0, 0.0, 1.0}, 
// 					{0.4, 1.0, 0.35}, {0.4, -1.0, 0.35}) << endl;
// }

