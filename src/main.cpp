#include <GLFW/glfw3.h>

#include "GL_utilities.h"
#include "LoadTGA.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>


#include "room.hpp"
#include "inputhandler.hpp"
#include "world.hpp"
#include "shader.hpp"
#include "projection.hpp"
#include "postprocessing.hpp"


using std::cout;
using std::endl;
using std::string;

const int TARGET_FPS = 30;

using namespace std::chrono;

GLFWwindow* window;
World world;
PostProcessor postProcessor;

int numFrames = 0;

float lastSPFTime;
void printSecondsPerFrame(float currentTime) {
	// SPF counter (= 1/FPS counter)
	numFrames++;
	if(currentTime-lastSPFTime >= 1.0){
		cout << (1000.0/float(numFrames)) << " ms/frame" << endl;
		numFrames = 0;
		lastSPFTime += 1.0;
	} 
}
 
void init(void) {
	// GL inits
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	
	shaderReg.compileShaders();
	printError("GLSL compiler error");
	
	postProcessor.init();
	printError("Init postprocessor");
	
	world.initRooms();
	calculateCameraAngles();
	
	printError("Init world");
}


// The main render loop
void display(void) {
	float lastTime = glfwGetTime();
	lastSPFTime = lastTime;
	
	while(!glfwWindowShouldClose(window)) {
		float startTime = glfwGetTime();
		float deltaTime = startTime-lastTime;
		lastTime = startTime;
// 		printSecondsPerFrame(startTime);
		
		Room* currentRoom = world.getCurrentRoom();
		
		glm::vec3 oldCamPos = glm::vec3(cam);
		glm::vec3 oldLookAt = glm::vec3(lookAtPoint);
		move(deltaTime);
		
		if(currentRoom->collides(oldCamPos, cam)) {
			// We have collided with something, go back to last valid position
			cam = oldCamPos;
			lookAtPoint = oldLookAt;
		}
	
		Portal* fromPortal = currentRoom->portalCollision(oldCamPos, cam);
		if(fromPortal != nullptr) {
			// We have collided with a portal, teleport
			Portal* toPortal = fromPortal->getToPortal();
			glm::mat4 fromM = fromPortal->getModelToWorldIn();
			glm::mat4 toM = toPortal->getModelToWorldOut();
			
			glm::mat4 totalM = toM * glm::inverse(fromM);
			
			cam = glm::vec3(totalM * glm::vec4(cam, 1.0));
			lookAtPoint = glm::vec3(totalM * glm::vec4(lookAtPoint, 1.0));
			calculateCameraAngles();
			world.setCurrentRoom(fromPortal->getToRoom());
			currentRoom = world.getCurrentRoom();
		}
		
		// Draw the world, starting from the current room
		postProcessor.activateHDRBuffer();
		glEnable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		currentRoom->drawRoom(cam, lookAtPoint);
		printError("In display");
		
		postProcessor.renderToScreen();
		printError("Render to screen");

		glfwSwapBuffers(window);
		
		// Make sure we don't render faster than target FPS.
		float endTime = glfwGetTime();
		if(endTime-startTime < 1.0/TARGET_FPS) {
			int sleepTime = 1000.0 * ((1.0/TARGET_FPS) - (endTime-startTime));
			std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
		}
	}
}


void resizeCallback(GLFWwindow* window, int width, int height) {
	screenHeight = height;
	screenWidth = width;
	glViewport(0, 0, width, height);
	postProcessor.resize(width, height);
}


void mouseCallback(GLFWwindow* window, double x, double y) {
	mouseX = (int)x;
	mouseY = (int)y;
}


void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if(key == GLFW_KEY_B and action == GLFW_PRESS)
		postProcessor.toggleBloom();
}


int main(int argc, char** argv) {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(1280, 720, "Portal", nullptr/*glfwGetPrimaryMonitor()*/, nullptr);
	glfwGetWindowSize(window, &screenWidth, &screenHeight);
	
	glfwMakeContextCurrent(window);
	if(window == nullptr) {
		cerr << "Error, failed to create window" << endl;
		glfwTerminate();
		return -1;
	}
	
	init();
	
	glfwSetCursorPos(window, 300.0, 300.0);
	glfwSetCursorPosCallback(window, mouseCallback);
	glfwSetFramebufferSizeCallback(window, resizeCallback);
	glfwSetKeyCallback(window, keyboardCallback);
	
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	
	// Run the main loop
	display();
	
	glfwTerminate();
	return 0;
}
