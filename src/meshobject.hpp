/**
 * A module containing classes for storing the mesh data for 3D 
 * models used for drawing or checking collisions.
 */

#pragma once

#include <iostream>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <vector>
#include <unordered_map>

#include "intersectiontest.hpp"

using std::string;
using std::vector;
using std::cerr;
using std::endl;


/**
 * A mesh object that can be drawn on the screen. Contains only the 
 * vertex and triangle information and keeps track of the OpenGL ids
 * for these buffers on the GPU. Supports vertices, uvs and normals. 
 */
class MeshObject {
	uint vao;
	uint vertexBuffer, normalBuffer, uvBuffer;
	uint indexBuffer;
public:
	vector<uint> indecies;
	vector<glm::vec3> vertices;
	vector<glm::vec3> normals;
	vector<glm::vec2> uvs;
	
	MeshObject() {};
	~MeshObject() = default;
	
	
	/**
	 * Upload this model to the GPU. This must be called before any
	 * calls to MeshObject::drawModel is done.
	 */ 
	void uploadModelToGPU() {
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		
		// Buffer for vertices
		glGenBuffers(1, &vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
		
		// Buffer for normals
		if(not normals.empty()) {
			glGenBuffers(1, &normalBuffer);
			glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
			glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
		}
		
		// Buffer for uvs
		if(not uvs.empty()) {
			glGenBuffers(1, &uvBuffer);
			glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
			glBufferData(GL_ARRAY_BUFFER, uvs.size()*sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
		}
		
		// Buffer for vertex indecies
		glGenBuffers(1, &indexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indecies.size()*sizeof(uint), &indecies[0], GL_STATIC_DRAW);
	}
	
	
	/**
	 * Draw this model using a shader program with a given names for vertex variable, 
	 * normal variable and texture coordinate variables.
	 */ 
	void drawModel(int program, const char *vertexVariableName, 
				   const char *normalVariableName, const char *texCoordVariableName) {
		
		glBindVertexArray(vao);	// Select VAO
		
		int loc = glGetAttribLocation(program, vertexVariableName);
		if(loc >= 0) {
			glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
			glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0); 
			glEnableVertexAttribArray(loc);
		}
		else
			cerr << "DrawModel, cannot find " << vertexVariableName << 
						" in shader program " << program << "." << endl;
		
		if (normalVariableName != NULL) {
			loc = glGetAttribLocation(program, normalVariableName);
			if (loc >= 0) {
				glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
				glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);
				glEnableVertexAttribArray(loc);
			}
			else
				cerr << "DrawModel, cannot find " << normalVariableName << 
						" in shader program " << program << "." << endl;
		}
	
		if ((not uvs.empty()) and (texCoordVariableName != NULL)) {
			loc = glGetAttribLocation(program, texCoordVariableName);
			if (loc >= 0) {
				glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
				glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
				glEnableVertexAttribArray(loc);
			}
			else
				cerr << "DrawModel, cannot find " << texCoordVariableName << 
						" in shader program " << program << "." << endl;
		}

		glDrawElements(GL_TRIANGLES, indecies.size(), GL_UNSIGNED_INT, 0L);
	}
};


/**
 * A collision proxy object, defined in world coordinates. Is used to test
 * collisions on.
 */
class CollisionObject {
public:
	vector<uint> indecies;
	vector<glm::vec3> vertices;
	
	/**
	* Builds a collision proxy by copying the vertices of a base mesh. 
	* The modelToWorld transformation will be applied to all vertices 
	* before they are added to this object.
	*/
	CollisionObject(const MeshObject *mesh, glm::mat4 modelToWorld) : indecies{mesh->indecies} {
		for(const glm::vec3 &vertex : mesh->vertices) {
			vertices.push_back(glm::vec3(modelToWorld*glm::vec4(vertex, 1.0)));
		}
	};
	CollisionObject() {};
	~CollisionObject() = default;
	
	/**
	 * Apply the given transformation matrix on all vertices in this
	 * collision object.
	 */
	void applyTransformation(const glm::mat4 &mat) {
		for(glm::vec3 &v : vertices)
			v = glm::vec3(mat*glm::vec4(v, 1.0));
	}
	
	/**
	 * Checks if a given line collides with any triangle in this collision mesh.
	 */
	bool collidesWithLine(const glm::vec3 &lineFrom, const glm::vec3 &lineTo) {
		for(uint i = 0; i < indecies.size(); i+=3) {
			if(lineTriangleHit(vertices[indecies[i]], vertices[indecies[i+1]], 
				vertices[indecies[i+2]], lineFrom, lineTo))
				return true;
		}
		return false;
	}
	
};
