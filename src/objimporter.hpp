/**
 * A simple Wavefront .obj importer.
 * Supports obj-files with vertices, uvs and normals.
 * Note that it only supports importing triangles, not quads.
 */

#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include "meshobject.hpp"

using std::string;
using std::vector;


/**
 * Import a obj file as a MeshObject instance. The importer
 * includes normals and texture coordinates (uvs) if they are 
 * included in the file.
 * 
 * Note: the importer only supports importing trinagles and quads, 
 * 		not arbitrary n-gons.
 */
MeshObject* importOBJ(const string &filename) {
	// Map of all unique vertex+uv+normal combinations -> id
	std::unordered_map<string, uint> vertexMap;
	vector<glm::vec3> vertices;
	vector<glm::vec3> normals;
	vector<glm::vec2> uvs;
	uint counter = 0;
	
	MeshObject* mesh = new MeshObject();
	std::ifstream objstream(filename);
	if(!objstream.is_open()){
		std::cerr << "ERROR: Could not open file " << filename << std::endl;
		return nullptr;
	}
	string line;
	while(std::getline(objstream, line)){
		if(line[0] == '#')
			continue;
		
		std::stringstream ss(line);
		string header;
		ss >> header;
		if(header == "v"){
			glm::vec3 vertex;
			ss >> vertex.x >> vertex.y >> vertex.z;
			vertices.push_back(vertex);
			
		}
		else if(header == "vt"){
			glm::vec2 uv;
			ss >> uv.x >> uv.y;
			uv.y = 1.0f -uv.y; //OpenGL coordinates are flipped compared to image file
			uvs.push_back(uv);
			
		}
		else if(header == "vn"){
			glm::vec3 normal;
			ss >> normal.x >> normal.y >> normal.z;
			normals.push_back(normal);
			
		}
		else if(header == "f"){
			// TODO: Generate normals if not in file?
			string vertexCode; 
			for(uint i = 0; i < 3; ++i) {
				ss >> vertexCode;
				uint id = counter;
				
				const auto &v = vertexMap.find(vertexCode);
				if(v == end(vertexMap)) {
					vertexMap[vertexCode] = counter;
					counter++;
				}
				else {
					id = vertexMap[vertexCode];
				}
				mesh->indecies.push_back(id);
			}
			
			if(ss >> vertexCode) {
				// This is a quad, triangle 0->1->2 already added, now we 
				// add triangle 2->3->0
				uint firstVert = mesh->indecies[mesh->indecies.size()-3];
				mesh->indecies.push_back(mesh->indecies.back());
				
				uint id = counter;
				const auto &v = vertexMap.find(vertexCode);
				if(v == end(vertexMap)) {
					vertexMap[vertexCode] = counter;
					counter++;
				}
				else {
					id = vertexMap[vertexCode];
				}
				mesh->indecies.push_back(id);
				mesh->indecies.push_back(firstVert);
			}
		}
	}
	
	
	mesh->vertices.resize(vertexMap.size());
	if(not normals.empty())
		mesh->normals.resize(vertexMap.size());
	if(not uvs.empty())
		mesh->uvs.resize(vertexMap.size());
	
	for(const auto &vert : vertexMap) {
		// Process one corner of a triangle of obj definition, like:
		// "1"   or   "1/2"    or   "1//2"    or   "1/2/3"
		std::stringstream ss(vert.first);
		uint vertexId = vert.second;
		
		uint tmp;
		ss >> tmp;
		mesh->vertices[vertexId] = vertices[tmp-1]; // .obj is 1 indexed
		if(ss.peek() == '/'){
			ss.ignore();
			if(ss.peek() == '/') {
				ss.ignore();
				ss >> tmp;
				mesh->normals[vertexId] = normals[tmp-1];
			}
			else{
				ss >> tmp;
				mesh->uvs[vertexId] = uvs[tmp-1];
				
				if(ss.peek() == '/') {
					ss.ignore();
					ss >> tmp;
					mesh->normals[vertexId] = normals[tmp-1];
				}
			}
		}
	}
	
	return mesh;
}


/**
 * Import a obj file as a CollisionObject for collision testing.
 * This importer ignores normal and texture coordinate data.
 * 
 * Note: the importer only supports importing trinagles and quads, 
 * 		not arbitrary n-gons.
 */
CollisionObject* importProxyOBJ(const string &filename) {
	CollisionObject* mesh = new CollisionObject();
	
	std::ifstream objstream(filename);
	if(!objstream.is_open()){
		std::cerr << "ERROR: Could not open file " << filename << std::endl;
		return nullptr;
	}
	string line;
	while(std::getline(objstream, line)){
		if(line[0] == '#')
			continue;
		
		std::stringstream ss(line);
		string header;
		ss >> header;
		if(header == "v"){
			glm::vec3 vertex;
			ss >> vertex.x >> vertex.y >> vertex.z;
			mesh->vertices.push_back(vertex);
		}
		else if(header == "f"){
			uint id;
			string vertexCode; 
			for(uint i = 0; i < 3; ++i) {
				ss >> id;
				if(ss.peek() == '/')
					ss >> vertexCode; // Ignore rest of 1/2/3 face definition
				mesh->indecies.push_back(id-1); // obj uses 1-based indexing
			}
			
			if(ss >> id) {
				// This is a quad, triangle 0->1->2 already added, now we 
				// add triangle 2->3->0
				uint firstVert = mesh->indecies[mesh->indecies.size()-3];
				mesh->indecies.push_back(mesh->indecies.back());
				mesh->indecies.push_back(id-1);
				mesh->indecies.push_back(firstVert);
			}
		}
	}
	return mesh;
}


