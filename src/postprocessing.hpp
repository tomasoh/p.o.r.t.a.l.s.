/**
 * Class for postprocessing step. Manages the HDR framebuffer object and
 * is responsible for drawing the HDR framebuffer to the screen buffer.
 */

#pragma once

#include <iostream>

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include "shader.hpp"

const uint BLOOM_BLUR_LEVELS = 3;

class PostProcessor {
	uint hdrFramebuffer;
	uint colorTextures[2];
	uint depthStencilBuffer;
	
	uint pingPongFramebuffers[2*BLOOM_BLUR_LEVELS];
	uint pingTextures[BLOOM_BLUR_LEVELS];
	uint pongTextures[BLOOM_BLUR_LEVELS];
	
	uint quadVAO;
	
	bool useBloom = true;
	
	// Sets up the HDR framebuffer object
	void initHDRFramebuffer() {
		glGenFramebuffers(1, &hdrFramebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, hdrFramebuffer);
		
		// Add default color texure and bright texture
		glGenTextures(2, colorTextures);
		for(uint i = 0; i < 2; ++i) {
			glBindTexture(GL_TEXTURE_2D, colorTextures[i]);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, screenWidth, screenHeight, 0, GL_RGBA, GL_FLOAT, nullptr);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorTextures[i], 0);
		}
		
		// Add combined depth and stencil buffer
		glGenRenderbuffers(1, &depthStencilBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthStencilBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, screenWidth, screenHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthStencilBuffer);
		
		uint outBuffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
		glDrawBuffers(2, outBuffers);
		
		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			std::cerr << "Error: PostProcessor failed to build HDR frame buffer" << std::endl; 
		}
		
		// Use default framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
	
	// Initializes the vertex array object for a quad covering the whole screen
	void initQuadMesh() {
		// Generate a quad mesh
		vector<glm::vec3> quadVertices = {{-1.0, 1.0, 0.0}, {-1.0, -1.0, 0.0}, {1.0, 1.0, 0.0}, {1.0, -1.0, 0.0}};
		vector<glm::vec2> quadTexCoords = {{0.0, 1.0}, {0.0, 0.0}, {1.0, 1.0}, {1.0, 0.0}};
		glGenVertexArrays(1, &quadVAO);
		glBindVertexArray(quadVAO);
		
		// Vertices for quad
		uint vertexBuffer;
		glGenBuffers(1, &vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, quadVertices.size()*sizeof(glm::vec3), &quadVertices[0], GL_STATIC_DRAW);
		int loc = glGetAttribLocation(shaderReg.hdrPostProcessId, "inPosition");
		glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0); 
		glEnableVertexAttribArray(loc);
		
		// UVs for quad
		uint uvBuffer;
		glGenBuffers(1, &uvBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, uvBuffer);
		glBufferData(GL_ARRAY_BUFFER, quadTexCoords.size()*sizeof(glm::vec2), &quadTexCoords[0], GL_STATIC_DRAW);
		loc = glGetAttribLocation(shaderReg.hdrPostProcessId, "inTexCoord");
		glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(loc);
		
		glBindVertexArray(0);
	}
	
	void drawQuad() {
		glBindVertexArray(quadVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}
	
	
	/**
	 * Sets up a texture for bluring in the bloom filter 
	 * scaleFactor is a number by which the screen size is divided to create
	 * the texture.
	 */
	void initBloomPingPongTextures(uint texture, int scaleFactor) {
		glBindTexture(GL_TEXTURE_2D, texture);
		uint width = screenWidth / scaleFactor; 
		uint height = screenHeight / scaleFactor;
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
	}
	
	void initBloomFramebuffers() {
		glGenFramebuffers(2*BLOOM_BLUR_LEVELS, pingPongFramebuffers);
		glGenTextures(BLOOM_BLUR_LEVELS, pingTextures);
		glGenTextures(BLOOM_BLUR_LEVELS, pongTextures);
		
		for(uint i = 0; i < BLOOM_BLUR_LEVELS; ++i) {
			uint scaleFactor = (1<<i);
			// Init ping textures
			glBindFramebuffer(GL_FRAMEBUFFER, pingPongFramebuffers[i*2]);
			initBloomPingPongTextures(pingTextures[i], scaleFactor);
			if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
				std::cerr << "Error: PostProcessor failed to build Bloom frame buffer" << std::endl; 

			// Init pong textures
			glBindFramebuffer(GL_FRAMEBUFFER, pingPongFramebuffers[i*2+1]);
			initBloomPingPongTextures(pongTextures[i], scaleFactor);
			if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
				std::cerr << "Error: PostProcessor failed to build Bloom frame buffer" << std::endl; 
		}
	}
	
	void blurBloomTexture() {
		glUseProgram(shaderReg.blurPostProcessId);
		glUniform1i(glGetUniformLocation(shaderReg.blurPostProcessId, "image"), 0);
		glActiveTexture(GL_TEXTURE0);
		
		for(uint i = 0; i < BLOOM_BLUR_LEVELS; ++i) {
			int sizeFactor = (1<<i);
			glUniform1i(glGetUniformLocation(shaderReg.blurPostProcessId, "sizeFactor"), sizeFactor);
			// Ping
			glBindFramebuffer(GL_FRAMEBUFFER, pingPongFramebuffers[i*2]);
			glUniform1i(glGetUniformLocation(shaderReg.blurPostProcessId, "directionX"), true);
			glBindTexture(GL_TEXTURE_2D, colorTextures[1]);
			drawQuad();
			
			// Pong
			glBindFramebuffer(GL_FRAMEBUFFER, pingPongFramebuffers[i*2+1]);
			glUniform1i(glGetUniformLocation(shaderReg.blurPostProcessId, "directionX"), false);
			glBindTexture(GL_TEXTURE_2D, pingTextures[i]);
			drawQuad();
		}
	}
	
public:
	PostProcessor() {};
	~PostProcessor() = default;
	
	
	void init() {
		initHDRFramebuffer();
		initBloomFramebuffers();
		printError("init HDR framebuffers:");
		initQuadMesh();
		printError("init quad mesh:");
	}
	

	void activateHDRBuffer() {
		glBindFramebuffer(GL_FRAMEBUFFER, hdrFramebuffer);
	}
	
	void toggleBloom() {
		useBloom = !useBloom;
	}
	
	// Call this when the window is resized to update the textures
	void resize(int width, int height) {
		// Update HDR framebuffer image sizes
		for(uint i = 0; i < 2; ++i) {
			glBindTexture(GL_TEXTURE_2D, colorTextures[i]);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
		}
		glBindRenderbuffer(GL_RENDERBUFFER, depthStencilBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
		
		// Update blur ping-pong buffer sizes
		for(uint i = 0; i < BLOOM_BLUR_LEVELS; ++i) {
			uint pingPongWidth = screenWidth / (1<<i); 
			uint pingPongHeight = screenHeight / (1<<i);
			
			glBindTexture(GL_TEXTURE_2D, pingTextures[i]);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, pingPongWidth, pingPongHeight, 0, GL_RGB, GL_FLOAT, nullptr);
			
			glBindTexture(GL_TEXTURE_2D, pongTextures[i]);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, pingPongWidth, pingPongHeight, 0, GL_RGB, GL_FLOAT, nullptr);
		
			// Clear the ping pong buffers
			glBindFramebuffer(GL_FRAMEBUFFER, pingPongFramebuffers[i*2]);
			glClear(GL_COLOR_BUFFER_BIT);
			glBindFramebuffer(GL_FRAMEBUFFER, pingPongFramebuffers[i*2+1]);
			glClear(GL_COLOR_BUFFER_BIT);
		}
	}
	
	// Renders the HDR framebuffer to screen
	void renderToScreen() {
		if(useBloom)
			blurBloomTexture();
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDisable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT);
		
		glUseProgram(shaderReg.hdrPostProcessId);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorTextures[0]);
		if(useBloom) {
			for(uint i = 0; i < BLOOM_BLUR_LEVELS; ++i) {
				glActiveTexture(GL_TEXTURE1 + i);
				glBindTexture(GL_TEXTURE_2D, pongTextures[i]);
			}
		}
		glUniform1i(glGetUniformLocation(shaderReg.hdrPostProcessId, "hdrTexture"), 0);
		glUniform1i(glGetUniformLocation(shaderReg.hdrPostProcessId, "bloomTexture"), 1);
		glUniform1i(glGetUniformLocation(shaderReg.hdrPostProcessId, "bloomTexture1"), 2);
		glUniform1i(glGetUniformLocation(shaderReg.hdrPostProcessId, "bloomTexture2"), 3);
		glUniform1i(glGetUniformLocation(shaderReg.hdrPostProcessId, "useBloom"), useBloom);
		
		drawQuad();
	}
	
};



