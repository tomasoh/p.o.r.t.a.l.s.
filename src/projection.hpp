#pragma once
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>

int screenHeight = 600;
int screenWidth = 600;

glm::mat4 getParallelFrustum(){
	return glm::perspective(glm::radians(90.0f), screenWidth/(float)screenHeight, 0.01f, 100.0f);
}


inline float signum(float a){
	if(a > 0.0f) return 1.0f;
	if(a < 0.0f) return -1.0f;
	return 0.0f;
}

/**
 * Implemented from Oblique View Frustum Depth Projection and Clipping by Eric Lengyel
 * 
 * @param normal The normal of the near clipping plane
 * @param point A point in the near clipping plane
 */
glm::mat4 getObliqueFrustum(const glm::vec4& normal, const glm::vec4& point) {
	glm::vec4 C{normal};
	C[3] = glm::dot(-normal, point);
	
	glm::mat4 ret = getParallelFrustum();
	glm::mat4 MInv = glm::inverse(ret);
	glm::vec4 Cp = glm::transpose(MInv) * C;
	glm::vec4 M4{0.0f, 0.0f, -1.0f, 0.0f};
	glm::vec4 Q = MInv * glm::vec4(signum(Cp[0]), signum(Cp[1]), 1.0f, 1.0f);
	glm::vec4 M3 = ((2.0f * dot(M4, Q))/dot(C, Q)) * C - M4;
	ret[0][2] = M3[0];
	ret[1][2] = M3[1];
	ret[2][2] = M3[2];
	ret[3][2] = M3[3];
	return ret;
}
