#pragma once

#include <glm/mat4x4.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <iostream>
#include "GL_utilities.h"

#include "meshobject.hpp"
#include "projection.hpp"
#include "shader.hpp"
#include "postprocessing.hpp"

const int MAX_LIGHTS = 8; // Keep this in sync with define in shaders
const int SHADOW_SIZE = 1024;
const float SHADOW_NEAR = 0.1f;
const float SHADOW_FAR = 25.0f;

extern PostProcessor postProcessor;

class Portal;
class Room;


/**
 * A struct contining the lighting information for a room.
 * Can contain two types of light:
 * - Point lights without shadow
 * - Point lights with shadow
 * Point lights with shadows are first in all vectors.
 */
struct LightData {
	// Lights with shadows are first in these three vector
	vector<glm::vec3> lightPositions;
	vector<glm::vec3> lightColors;
	vector<float> lightIntensities;
	
	vector<uint> lightShadowCubeMaps;
	vector<uint> lightShadowFramebuffer;
	
	// Add a shadow light to this struct and set up the shadow texture and framebuffer
	void addShadowLight(glm::vec3 position, glm::vec3 color, float intensity){
		lightPositions.insert(begin(lightPositions), position);
		lightColors.insert(begin(lightColors), color);
		lightIntensities.insert(begin(lightIntensities), intensity);
		
		uint framebufferId;
		glGenFramebuffers(1, &framebufferId);
		
		// Generate the cube map texture
		uint cubeMap;
		glGenTextures(1, &cubeMap);
		lightShadowCubeMaps.insert(begin(lightShadowCubeMaps), cubeMap);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);
		for(int i = 0; i < 6; ++i)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_SIZE, SHADOW_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		
		// Attach texture to framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, framebufferId);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, cubeMap, 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		lightShadowFramebuffer.insert(begin(lightShadowFramebuffer), framebufferId);
	}
	
	// Add a non-shadowy light to this struct
	void addLight(glm::vec3 position, glm::vec3 color, float intensity) {
		lightPositions.push_back(position);
		lightColors.push_back(color);
		lightIntensities.push_back(intensity);
	}
	
	// The number of lights (both with and without shadows) in this struct
	size_t size() const {
		return lightPositions.size();
	}
	
	bool empty() const {
		return lightPositions.empty();
	}
};


/**
 * An instance of an object. This class can share a base mesh, but with 
 * a unique model to world transformation matrix. The ObjectInstance can also
 * use unique textures and a unique shader program.
 */
class ObjectInstance {
	/**
	 * An instance of an object in a room.
	 */
	MeshObject* mesh;
	glm::mat4 modelToWorld;
	vector<uint> textureIds;
	ShaderProgram* program;
	
public:
	// Material specification for shader
	float materialSpecularHardness = 32.0;
	float materialDiffuse = 0.8;
	float materialSpecularity = 0.8;
	float materialAmbient = 0.2;
	// Only for emission shader:
	float materialLightIntensity = 3.0;
	
	ObjectInstance(MeshObject* _mesh, glm::mat4 _modelToWorld, ShaderProgram* _shaderProgram) : 
			mesh{_mesh}, modelToWorld{_modelToWorld}, program{_shaderProgram} {};
	ObjectInstance(const ObjectInstance& other) : mesh{other.mesh}, modelToWorld{other.modelToWorld}, 
			textureIds{other.textureIds}, program{other.program} {};
	~ObjectInstance();
	
	/**
	 * Textures added will be mapped to sampler2D named textureSampler0, 
	 * textureSampler1, textureSamp... in the same order they are added.
	 */
	void addTexture(uint texId) {
		textureIds.push_back(texId);
	}
	
	const glm::mat4 getModelToWorld() const {
		return modelToWorld;
	}
	
	void setModelToWorld(glm::mat4 mtw){
		modelToWorld = mtw;
	}
	
	void drawModel(const glm::vec3& viewPos, const glm::mat4& worldToView, 
				   const glm::mat4& viewToProj, const LightData& lights) {
		glUseProgram(program->programId);
		
		for(uint i = 0; i < textureIds.size(); ++i) {
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, textureIds[i]);
			string s = "textureSampler";
			s.push_back('0' + i);
			glUniform1i(glGetUniformLocation(program->programId, s.c_str()), i);
		}
		
		int numShadowLightsId = glGetUniformLocation(program->programId, "numShadowLights");
		// Add shadow casting lights
		if(numShadowLightsId >= 0) {
			if(lights.lightShadowCubeMaps.size() > 1)
				std::cerr << "ERROR (ObjectInstance::drawModel): Tried to use more than one shadow lamp in a room! For now we only support max one such lamp per room!" << std::endl;
			glUniform1i(numShadowLightsId, lights.lightShadowCubeMaps.size());
			for(uint i = 0; i < lights.lightShadowCubeMaps.size(); ++i) {
				glActiveTexture(GL_TEXTURE0 + i + textureIds.size());
				glBindTexture(GL_TEXTURE_CUBE_MAP, lights.lightShadowCubeMaps[i]);
				glUniform1i(glGetUniformLocation(program->programId, "lightDepthMap"), i + textureIds.size());
			}
		}
		
		// Add regular lights
		int numLightsId = glGetUniformLocation(program->programId, "numLights");
		if(!lights.empty() and numLightsId >= 0) {
			glUniform1i(numLightsId, lights.size());
			glUniform3fv(glGetUniformLocation(program->programId, "lightPositions[0]"), lights.size(), &lights.lightPositions[0][0]);
			glUniform3fv(glGetUniformLocation(program->programId, "lightColors[0]"), lights.size(), &lights.lightColors[0][0]);
			glUniform1fv(glGetUniformLocation(program->programId, "lightIntensities[0]"), lights.size(), &lights.lightIntensities[0]);
		}
		
		int specularHardnessId = glGetUniformLocation(program->programId, "materialSpecularHardness");
		if(specularHardnessId >= 0) {
			glUniform1f(specularHardnessId, materialSpecularHardness);
			glUniform1f(glGetUniformLocation(program->programId, "materialDiffuse"), materialDiffuse);
			glUniform1f(glGetUniformLocation(program->programId, "materialSpecularity"), materialSpecularity);
			glUniform1f(glGetUniformLocation(program->programId, "materialAmbient"), materialAmbient);
		}
		
		int lightIntensityId = glGetUniformLocation(program->programId, "lightIntensity");
		if(lightIntensityId >= 0)
			glUniform1f(lightIntensityId, materialLightIntensity);
		
		glm::mat4 modelToView = worldToView * modelToWorld;
		glm::mat4 modelToProj = viewToProj * modelToView;
		glUniformMatrix4fv(glGetUniformLocation(program->programId, "modelToWorld"), 1, GL_FALSE, &modelToWorld[0][0]);
		glUniformMatrix4fv(glGetUniformLocation(program->programId, "modelToView"), 1, GL_FALSE, &modelToView[0][0]);
		glUniformMatrix4fv(glGetUniformLocation(program->programId, "modelToProj"), 1, GL_FALSE, &modelToProj[0][0]);
		glUniform3fv(glGetUniformLocation(program->programId, "viewPosition"), 1, &viewPos[0]);
		
		
		mesh->drawModel(program->programId, program->verticesName, program->normalsName, program->uvsName);
	}
	
	// Draw this model with a custom shader program. Normally use drawModel(...) method instead
	void drawModelCustom(int program, const char *vertexVariableName, 
				   const char *normalVariableName, const char *texCoordVariableName) {
		mesh->drawModel(program, "position", nullptr, nullptr);
	}
	
};


/**
 * A portal in a room. The portal connects the room it is in to another 
 * portal (in another or the same room). The portal has one in-side and one
 * out-side, that are on the oposite sides of one another. The out-side is
 * the side another portal will lead to when connected to a portal. The in-side
 * is optional, and if it is used, it will be the side where we can see through
 * and walk through the portal.
 */
class Portal {
	MeshObject* portalMesh;
	MeshObject* portalMesh2;
	CollisionObject* proxyMesh;
	glm::mat4 modelToWorldIn;
	glm::mat4 modelToWorldOut;
	Room* toRoom = nullptr;
	Portal* toPortal = nullptr;
	int shaderProgram;
public:
	
	/**
	 * Create a portal plane instance. A portal is never visible on the screen.
	 *
	 * mesh 				The mesh of the plane. This must be pointing i -Y direction.
	 * _modelToWorld 		The model to world matrix for this portal.
	 * outAndInOnSameSide	Tells wich direction the "out-side" of the portal plane is. If
	 *          false, teleporting to this portal will mean that the camera moves to the back 
	 *          side of the portal, true means the camera moves to the same side as the 
	 *          portals "in-side".
	 * program				A simple shader program that draws the portal mesh. Will only be 
	 *          used to draw to stencil buffer.
	 */
	Portal(MeshObject *mesh, glm::mat4 _modelToWorld, bool outAndInOnSameSide, int program) : 
					portalMesh{mesh}, modelToWorldIn{_modelToWorld}, shaderProgram{program} {
		
		proxyMesh = new CollisionObject(portalMesh, modelToWorldIn);
		
		portalMesh2 = new MeshObject();
		for(uint index : portalMesh->indecies)
			portalMesh2->indecies.push_back(index);
		
		// Copy the plane and move it 2 cm back to prevent flickering when teleporting
		glm::mat4 trans = glm::translate(glm::mat4(1.0), glm::vec3(0.0, 0.02, 0.0));
		for(glm::vec3 vert : portalMesh->vertices)
			portalMesh2->vertices.push_back(glm::vec3(trans*glm::vec4(vert, 1.0)));
		
		if(outAndInOnSameSide) 
			modelToWorldOut = modelToWorldIn*glm::rotate(glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		else
			modelToWorldOut = modelToWorldIn;
		
		portalMesh2->uploadModelToGPU();
	};
	~Portal() {};
	
	/**
	 * Get the modelToWorld matrix for this portal as in this room.
	 */
	glm::mat4 getModelToWorldIn() const {
		return modelToWorldIn;
	}
	
	/**
	 * Get the modelToWorld matrix for this portal as seen through the portal
	 * from another room (out of the portal). Might be same as getModelToWorldIn().
	 */
	glm::mat4 getModelToWorldOut() const {
		return modelToWorldOut;
	}
	
	void setToPortal(Room *_toRoom, Portal *_toPortal) {
		toRoom = _toRoom;
		toPortal = _toPortal;
	}
	
	/**
	 * Returns toRoom, or nullptr if no such.
	 */
	Room* getToRoom() const {
		return toRoom;
	}
	
	/**
	 * Returns toPortal, or nullptr if no such. 
	 */
	Portal* getToPortal() const {
		return toPortal;
	}
	
	/**
	 * This portal is a in portal (we can see it, an teleport through it)
	 * If this is not a in-portal, we can only teleport to this portal, not from it.
	 */
	bool isInPortal() const {
		return toRoom != nullptr;
	}
	
	/**
	 * Test if a line between two points in world space collides with this
	 * portal. Only returns true if the line collides with the in-side of
	 * the portal, not the out-side.
	 */
	bool collidesWithLine(const glm::vec3 &fromPoint, const glm::vec3 &toPoint) const {
		return proxyMesh->collidesWithLine(fromPoint, toPoint);
	}
	
	/**
	 * Draw this portal. (Only makes sense if only stencil buffer writing
	 * is active)
	 */
	void drawPortal(glm::vec3 cam, glm::mat4 worldToView, glm::mat4 viewToProj) const {
		glUseProgram(shaderProgram);
		glm::mat4 modelToView = worldToView * modelToWorldIn;
		glm::mat4 modelToProj = viewToProj * modelToView;
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "modelToProj"), 1, GL_FALSE, &modelToProj[0][0]);
		
		portalMesh->drawModel(shaderProgram, "inPosition", nullptr, nullptr);
		
		// Draw the back face only if we are standing in front of the front face
		if(isVisible(cam, worldToView))
			portalMesh2->drawModel(shaderProgram, "inPosition", nullptr, nullptr);
	}
	
	bool isVisible(const glm::vec3& cam, const glm::mat4& worldToView) const {
		glm::vec4 normal = glm::vec4(0.0, -1.0, 0.0, 0.0);
		glm::vec4 camIn = glm::inverse(modelToWorldIn)*glm::vec4(cam, 1.0);
		return glm::dot(normal, camIn) > 0.0;
	}
	
};


/**
 * A class representing a room in the worlds, filled with objects.
 * A room can have portals that teleport the player to or from other rooms.
 */
class Room {
protected:
	const int maxPortalDepth;
	vector<ObjectInstance*> objects;
	vector<CollisionObject*> collisionProxys;
	vector<Portal*> portals;
	
	LightData lights;
	
public:
	Room(int _maxPortalDepth=3) : maxPortalDepth{_maxPortalDepth} {};
	~Room() = default;
	
	/**
	 * Draw this room on the screen. 
	 * 
	 * cam:			The camera position in the world as it is in the last room.
	 * 				Same as the real camera position if fromPortal == nullptr
	 * lookAtPoint: The point the camera is facing in the last room, same as
	 * 				The real camera point if fromPortal == nullptr.
	 * portalDepth: The max number of recursive portal drawing calls left
	 * fromPortal: 	The in-portal in the room we are rendering from. nullptr 
	 * 				if we are in this room we are rendering.
	 */
	virtual void drawRoom(glm::vec3 cam, glm::vec3 lookAtPoint, int portalDepth = 0,
						  Portal* fromPortal = nullptr) {};

	/**
	 * Draw the objects in this room.
	 */
	virtual void drawObjectsInThisRoom(const glm::vec3& cam, const glm::mat4& worldToView, const glm::mat4& viewToProj, bool isFirstRoom) {
		for(ObjectInstance* instance : objects)
			instance->drawModel(cam, worldToView, viewToProj, lights);
	} 
	
	
	/**
	 * Adds a object to this room. (duh)
	 */
	virtual void addObject(ObjectInstance* instance) {
		objects.push_back(instance);
	}
	
	/**
	 * Adds a point light to this room.
	 */
	void addLight(glm::vec3 position, glm::vec3 color, float intensity) {
		lights.addLight(position, color, intensity);
		if(lights.size() > MAX_LIGHTS)
			std::cerr << "You have added to many lights to this room! Max is: " << MAX_LIGHTS << std::endl;
	}

	
	/**
	 * Renders (or re-renders) the shadow lamp depth texture for a shadow 
	 * point lamp with a specific index in the lightData struct.
	 * Only the objects in the shadowMeshes vactor is used to render the shadow depth.
	 */
	void renderLampShadowMaps(const LightData& lightData, uint index, const vector<ObjectInstance*>& shadowMeshes) {
		glm::vec3 position = lightData.lightPositions[index];
		glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), 1.0f, SHADOW_NEAR, SHADOW_FAR); 
		
		vector<glm::mat4> shadowTransforms;
		shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(1.0,0.0,0.0), glm::vec3(0.0,-1.0,0.0))); /* Right */
		shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(-1.0,0.0,0.0), glm::vec3(0.0,-1.0,0.0))); /* Left */
		shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,1.0,0.0), glm::vec3(0.0, 0.0, 1.0))); /* Back */
		shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,-1.0,0.0), glm::vec3(0.0,0.0,-1.0))); /* Front */
		shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,0.0,1.0), glm::vec3(0.0,-1.0,0.0))); /* Up */
		shadowTransforms.push_back(shadowProj * glm::lookAt(position, position + glm::vec3(0.0,0.0,-1.0), glm::vec3(0.0,-1.0,0.0))); /* Down */
		
		
		uint depthProgram = shaderReg.shadowDepthProgramId;
		glViewport(0, 0, SHADOW_SIZE, SHADOW_SIZE);
		glBindFramebuffer(GL_FRAMEBUFFER, lightData.lightShadowFramebuffer[index]);
		glClear(GL_DEPTH_BUFFER_BIT);
		glUseProgram(depthProgram);

		// Draw shadow depth to framebuffer
		for(int i = 0; i < 6; ++i){
			const string s = "shadowMatrices[" + std::to_string(i) + "]";
			glUniformMatrix4fv(glGetUniformLocation(depthProgram, s.c_str()), 1, GL_FALSE, &shadowTransforms[i][0][0]);
		}
		glUniform1f(glGetUniformLocation(depthProgram, "far_plane"), SHADOW_FAR);
		glUniform3fv(glGetUniformLocation(depthProgram, "lightPos"), 1, &position[0]);
		
		for(ObjectInstance* obj : shadowMeshes){
			glUniformMatrix4fv(glGetUniformLocation(depthProgram, "modelToWorld"), 1, GL_FALSE, &obj->getModelToWorld()[0][0]);
			obj->drawModelCustom(depthProgram, "position", nullptr, nullptr);
		}

		// Reset to use default framebuffer for online rendering to screen.
// 		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		postProcessor.activateHDRBuffer();
		glViewport(0, 0, screenWidth, screenHeight);
	}
	
	void updateShadowLightPos(int index, LightData& lightData, const glm::vec3& newLightPos) {
		lightData.lightPositions[index] = newLightPos;
	}
	
	
	/**
	 * Add a point light with shadowsa and render the shadow map for it.
	 */
	virtual void addShadowLight(glm::vec3 position, glm::vec3 color, float intensity) {
		lights.addShadowLight(position, color, intensity);
		renderLampShadowMaps(lights, 0, objects); // Last shadow map always has index 0
		if(lights.size() > MAX_LIGHTS)
			std::cerr << "You have added to many lights to this room! Max is: " << MAX_LIGHTS << std::endl;
	}
	
	/**
	 * Adds a collision proxy to this room. The collision proxys are
	 * queried for collisions when the player is in this room.
	 */
	void addCollisionProxy(CollisionObject* instance) {
		collisionProxys.push_back(instance);
	}
	
	/**
	 * Adds a portal to this room.
	 */
	void addPortal(Portal* portal) {
		portals.push_back(portal);
	}
	
	/**
	 * Checks if a line between two points collides with any object in this
	 * room.
	 */
	bool collides(const glm::vec3 &fromPoint, const glm::vec3 &toPoint) {
		for(CollisionObject* proxy : collisionProxys)
			if(proxy->collidesWithLine(fromPoint, toPoint))
				return true;
		return false;
	}
	
	/**
	 * Checks if a line between two points collides with any portal in this room.
	 * Returns a pointer to the portal it collides with, or nullptr if no
	 * 		portals collide with it.
	 */
	Portal* portalCollision(const glm::vec3 &fromPoint, const glm::vec3 &toPoint){
		for(Portal* p : portals){
			if(p->isInPortal() and p->collidesWithLine(fromPoint, toPoint))
				return p;
		}
		return nullptr;
		
	}
};
