#pragma once

#include <iostream>
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "glm/mat4x4.hpp"
#include <glm/gtx/rotate_vector.hpp>

#include "room.hpp"
#include "projection.hpp"

extern const glm::vec3 Z_AXIS;
extern const glm::vec3 Y_AXIS;
extern const glm::vec3 X_AXIS;
extern PostProcessor postProcessor;

class StandardRoom : public Room {

public:	
	StandardRoom(int _maxPortalDepth=3) : Room{_maxPortalDepth} {};
	
	void drawRoom(glm::vec3 cam, glm::vec3 lookAtPoint, int portalDepth = 0,
				  Portal* fromPortal = nullptr) override {

		glm::mat4 worldToView = glm::lookAt(cam, lookAtPoint, glm::vec3(0.0, 0.0, 1.0));
		glm::mat4 viewToProj;
		if(fromPortal) {
			// We are drawing this room as seen through the portal fromPortal
			Portal* toPortal = fromPortal->getToPortal();
			glm::mat4 fromM = fromPortal->getModelToWorldIn();
			glm::mat4 toM = toPortal->getModelToWorldOut();
			glm::mat4 totalM = toM * glm::inverse(fromM);
			
			// Recalculate the points we are seeing this room from
			cam = glm::vec3(totalM * glm::vec4(cam, 1.0));
			lookAtPoint = glm::vec3(totalM * glm::vec4(lookAtPoint, 1.0));
			
			worldToView = glm::lookAt(cam, lookAtPoint, glm::vec3(0.0, 0.0, 1.0));
			glm::vec4 point =  worldToView * toPortal->getModelToWorldOut() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			glm::vec4 normal = worldToView * toPortal->getModelToWorldOut() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
			viewToProj = getObliqueFrustum(normal, point);
		}
		else {
			viewToProj = getParallelFrustum();
		}

		// Draw this room
		drawObjectsInThisRoom(cam, worldToView, viewToProj, (fromPortal==nullptr));

		
		// Don't draw more portals if max limit is reached
		if(portalDepth >= maxPortalDepth)
			return;
		
		// Draw all rooms seen through portals recursively
		
		if(portalDepth == 0) {
			// Enable and clear stencil buffer
			glEnable(GL_STENCIL_TEST);
			glStencilMask(0xFF);
			glClear(GL_STENCIL_BUFFER_BIT);
		}
		
		vector<Portal*> visiblePortals;
		for(Portal* portal : portals) {
			if(fromPortal and portal == fromPortal->getToPortal())
				continue;
			
			if(!portal->isInPortal() or !portal->isVisible(cam, worldToView))
				continue;
			visiblePortals.push_back(portal);
		}

		// "Draw" the value portalDepth+1 to stencil buffer where stencil 
		// value is portalDepth using the portal meshes
		for(Portal* portal : visiblePortals) {
			glStencilFunc(GL_EQUAL, portalDepth, 0xFF); // Write where stencil is equal to portalDepth 
			glStencilOp(GL_KEEP, GL_KEEP, GL_INCR); // Increment if stencil and z-test passes
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // Disable drawing to screen
			glDepthMask(GL_FALSE); // Disable writing to Z-buffer
			glStencilMask(0xFF); // Enable writing to stencil buffer
			
			// Draw portal to stencil buffer using Z-buffer (but not writing to Z-buffer)
			portal->drawPortal(cam, worldToView, viewToProj);
		}
		
		
		// Draw each room as seen through the portals
		for(Portal* portal : visiblePortals) {
			// Enable drawing, disable stencil drawing
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			glDepthMask(GL_TRUE);
			glStencilMask(0x00);
			
			// Only draw where stencil is portalDepth+1
			glStencilFunc(GL_EQUAL, portalDepth+1, 0xFF);
			glClear(GL_DEPTH_BUFFER_BIT);
			
			// Draw what's seen through the portal
			portal->getToRoom()->drawRoom(cam, lookAtPoint, portalDepth+1, portal);
			
			// Reset the stencil buffer by overwriting this portal with 0s
			glStencilFunc(GL_NEVER, 0, 0xFF); // Always fail stencil test
			glStencilOp(GL_ZERO, GL_KEEP, GL_KEEP); // Write zero when stencil test fails
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // Disable drawing to screen
			glDepthMask(GL_FALSE); // Disable writing to Z-buffer
			glStencilMask(0xFF); // Enable writing to stencil buffer
			
			// Draw 0s where portal is
			portal->drawPortal(cam, worldToView, viewToProj);
		}
		
		// Re-enable drawing to color and depth, disable stencil writing
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDepthMask(GL_TRUE);
		glStencilMask(0x00);
		
		if(portalDepth == 0)
			glDisable(GL_STENCIL_TEST);
		
	}
	
};



const int INFINITY_ROOM_MAX_PORTAL_DEPTH = 10;

/**
 * A special kind of room for the infinity corridor. The corridor has
 * one portal out of the corridor, and two portals, one in each end of
 * the corridor that loops back to the other side of the corridor.
 * This is needed to make some optimizations tricks to draw as few rooms 
 * as possible.
 */
class InfinityRoom : public Room {
	Portal* outPortal;
	Portal* loopingLeftPortal;
	Portal* loopingRightPortal;
	
public:
	InfinityRoom() : Room{INFINITY_ROOM_MAX_PORTAL_DEPTH} {};
	
	// Adds a portal out of this room
	void addOuterPortal(Portal* portal) {
		outPortal = portal;
		portals.push_back(portal);
	}
	
	// Adds a portal that loops back to this corridor
	void addLoopingLeftPortal(Portal* portal) {
		loopingLeftPortal = portal;
		portals.push_back(portal);
	}
	
	// Adds a portal that loops back to this corridor
	void addLoopingRightPortal(Portal* portal) {
		loopingRightPortal = portal;
		portals.push_back(portal);
	}
	
	void drawRoom(glm::vec3 cam, glm::vec3 lookAtPoint, int portalDepth = 0,
				  Portal* fromPortal = nullptr) override {

		glm::mat4 worldToView = glm::lookAt(cam, lookAtPoint, glm::vec3(0.0, 0.0, 1.0));
		glm::mat4 viewToProj;
		if(fromPortal) {
			// We are drawing this room as seen through the portal fromPortal
			Portal* toPortal = fromPortal->getToPortal();
			glm::mat4 fromM = fromPortal->getModelToWorldIn();
			glm::mat4 toM = toPortal->getModelToWorldOut();
			glm::mat4 totalM = toM * glm::inverse(fromM);
			
			// Recalculate the points we are seeing this room from
			cam = glm::vec3(totalM * glm::vec4(cam, 1.0));
			lookAtPoint = glm::vec3(totalM * glm::vec4(lookAtPoint, 1.0));
			
			worldToView = glm::lookAt(cam, lookAtPoint, glm::vec3(0.0, 0.0, 1.0));
			glm::vec4 point =  worldToView * toPortal->getModelToWorldOut() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			glm::vec4 normal = worldToView * toPortal->getModelToWorldOut() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
			
			viewToProj = getObliqueFrustum(normal, point);
		}
		else {
			viewToProj = getParallelFrustum();
		}

		// Draw this room
		drawObjectsInThisRoom(cam, worldToView, viewToProj, (fromPortal==nullptr));

		// Don't draw more portals if max limit is reached
		if(portalDepth >= maxPortalDepth or fromPortal == outPortal->getToPortal())
			return;
		
		
		// Draw all rooms seen through portals recursively
		if(portalDepth == 0) {
			// Enable and clear stencil buffer
			glEnable(GL_STENCIL_TEST);
			glStencilMask(0xFF);
			glClear(GL_STENCIL_BUFFER_BIT);
		}
		
		vector<Portal*> visiblePortals;

		// Archway portal can only be seen in the current corridor.
		if(portalDepth == 0)
			visiblePortals.push_back(outPortal);
		
		// Only draw rest of corridor if we are looking in that direction
		glm::vec3 viewDir = lookAtPoint-cam;
		bool lookingPositiveY = glm::dot(viewDir, glm::vec3(0.0f, 1.0f, 0.0f)) > 0.0f;
		if((portalDepth <= 1 or !lookingPositiveY) and loopingLeftPortal->isVisible(cam, worldToView))
			visiblePortals.push_back(loopingLeftPortal);

		if((portalDepth <= 1 or lookingPositiveY) and loopingRightPortal->isVisible(cam, worldToView))
			visiblePortals.push_back(loopingRightPortal);
			
		if(visiblePortals.empty())
			return;
		
		// "Draw" the value portalDepth+1 to stencil buffer where stencil 
		// value is portalDepth using the portal meshes
		for(Portal* portal : visiblePortals) {
			glStencilFunc(GL_EQUAL, portalDepth, 0xFF); // Write where stencil is equal to portalDepth 
			glStencilOp(GL_KEEP, GL_KEEP, GL_INCR); // Increment if stencil and z-test passes
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // Disable drawing to screen
			glDepthMask(GL_FALSE); // Disable writing to Z-buffer
			glStencilMask(0xFF); // Enable writing to stencil buffer
			
			// Draw portal to stencil buffer using Z-buffer (but not writing to Z-buffer)
			portal->drawPortal(cam, worldToView, viewToProj);
		}
		
		
		// Draw each room as seen through the portals
		for(Portal* portal : visiblePortals) {
			// Enable drawing, disable stencil drawing
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			glDepthMask(GL_TRUE);
			glStencilMask(0x00);
			
			// Only draw where stencil is portalDepth+1
			glStencilFunc(GL_EQUAL, portalDepth+1, 0xFF);
			glClear(GL_DEPTH_BUFFER_BIT);
			
			// Draw what's seen through the portal
			portal->getToRoom()->drawRoom(cam, lookAtPoint, portalDepth+1, portal);
			
			// Reset the stencil buffer by overwriting this portal with 0s
			glStencilFunc(GL_NEVER, 0, 0xFF); // Always fail stencil test
			glStencilOp(GL_ZERO, GL_KEEP, GL_KEEP); // Write zero when stencil test fails
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // Disable drawing to screen
			glDepthMask(GL_FALSE); // Disable writing to Z-buffer
			glStencilMask(0xFF); // Enable writing to stencil buffer
			
			// Draw 0s where portal is
			portal->drawPortal(cam, worldToView, viewToProj);
		}
		
		// Re-enable drawing to color and depth, disable stencil writing
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDepthMask(GL_TRUE);
		glStencilMask(0x00);
		
		if(portalDepth == 0)
			glDisable(GL_STENCIL_TEST);
		
	}
	
};



/**
 * A special kind of room for the simple corridor to optimize drawing.
 */
class SimpleCorridorRoom : public Room {
	// Portals that can only be seen from inside the room
	vector<Portal*> hiddenPortals;
	// Portals that can only be seen from outside the room
	vector<Portal*> endPointPortals;
	
public:
	SimpleCorridorRoom(int _maxPortalDepth=3) : Room{_maxPortalDepth} {};
	
	// Adds a portal out of this room
	void addHiddenPortal(Portal* portal) {
		hiddenPortals.push_back(portal);
		portals.push_back(portal);
	}
	
	// Adds a portal out of this room
	void addEndPointPortal(Portal* portal) {
		endPointPortals.push_back(portal);
		portals.push_back(portal);
	}
	
	
	void drawRoom(glm::vec3 cam, glm::vec3 lookAtPoint, int portalDepth = 0,
				  Portal* fromPortal = nullptr) override {

		glm::mat4 worldToView = glm::lookAt(cam, lookAtPoint, glm::vec3(0.0, 0.0, 1.0));
		glm::mat4 viewToProj;
		if(fromPortal) {
			// We are drawing this room as seen through the portal fromPortal
			Portal* toPortal = fromPortal->getToPortal();
			glm::mat4 fromM = fromPortal->getModelToWorldIn();
			glm::mat4 toM = toPortal->getModelToWorldOut();
			glm::mat4 totalM = toM * glm::inverse(fromM);
			
			// Recalculate the points we are seeing this room from
			cam = glm::vec3(totalM * glm::vec4(cam, 1.0));
			lookAtPoint = glm::vec3(totalM * glm::vec4(lookAtPoint, 1.0));
			
			worldToView = glm::lookAt(cam, lookAtPoint, glm::vec3(0.0, 0.0, 1.0));
			glm::vec4 point =  worldToView * toPortal->getModelToWorldOut() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			glm::vec4 normal = worldToView * toPortal->getModelToWorldOut() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
			
			viewToProj = getObliqueFrustum(normal, point);
		}
		else {
			viewToProj = getParallelFrustum();
		}

		// Draw this room
		drawObjectsInThisRoom(cam, worldToView, viewToProj, (fromPortal==nullptr));

		// Don't draw more portals if max limit is reached
		if(portalDepth >= maxPortalDepth)
			return;
		
		
		// Draw all rooms seen through portals recursively
		if(portalDepth == 0) {
			// Enable and clear stencil buffer
			glEnable(GL_STENCIL_TEST);
			glStencilMask(0xFF);
			glClear(GL_STENCIL_BUFFER_BIT);
		}
		
		vector<Portal*> visiblePortals;
		for(Portal* portal : endPointPortals) {
			if(fromPortal and portal == fromPortal->getToPortal())
				continue;
			
			if(!portal->isInPortal() or !portal->isVisible(cam, worldToView))
				continue;
			visiblePortals.push_back(portal);
		}
		
		// "Hidden" portals can only be seen inside this room
		if(portalDepth == 0)
			visiblePortals.insert(end(visiblePortals), begin(hiddenPortals), end(hiddenPortals));
		
		
		// "Draw" the value portalDepth+1 to stencil buffer where stencil 
		// value is portalDepth using the portal meshes
		for(Portal* portal : visiblePortals) {
			glStencilFunc(GL_EQUAL, portalDepth, 0xFF); // Write where stencil is equal to portalDepth 
			glStencilOp(GL_KEEP, GL_KEEP, GL_INCR); // Increment if stencil and z-test passes
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // Disable drawing to screen
			glDepthMask(GL_FALSE); // Disable writing to Z-buffer
			glStencilMask(0xFF); // Enable writing to stencil buffer
			
			// Draw portal to stencil buffer using Z-buffer (but not writing to Z-buffer)
			portal->drawPortal(cam, worldToView, viewToProj);
		}
		
		
		// Draw each room as seen through the portals
		for(Portal* portal : visiblePortals) {
			// Enable drawing, disable stencil drawing
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			glDepthMask(GL_TRUE);
			glStencilMask(0x00);
			
			// Only draw where stencil is portalDepth+1
			glStencilFunc(GL_EQUAL, portalDepth+1, 0xFF);
			glClear(GL_DEPTH_BUFFER_BIT);
			
			// Draw what's seen through the portal
			portal->getToRoom()->drawRoom(cam, lookAtPoint, portalDepth+1, portal);
			
			// Reset the stencil buffer by overwriting this portal with 0s
			glStencilFunc(GL_NEVER, 0, 0xFF); // Always fail stencil test
			glStencilOp(GL_ZERO, GL_KEEP, GL_KEEP); // Write zero when stencil test fails
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); // Disable drawing to screen
			glDepthMask(GL_FALSE); // Disable writing to Z-buffer
			glStencilMask(0xFF); // Enable writing to stencil buffer
			
			// Draw 0s where portal is
			portal->drawPortal(cam, worldToView, viewToProj);
		}
		
		// Re-enable drawing to color and depth, disable stencil writing
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glDepthMask(GL_TRUE);
		glStencilMask(0x00);
		
		if(portalDepth == 0)
			glDisable(GL_STENCIL_TEST);
		
	}
	
};



class CanadaRoom : public StandardRoom {
	ObjectInstance* head;
	ObjectInstance* body;
	
	glm::mat4 headMatrix(float time) {
		int t = (int)(time*8);
		glm::mat4 ret = glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 0.75f));
		if(t%4 == 0){
			ret = ret * glm::rotate(glm::mat4(1.0f), glm::radians(10.0f), Y_AXIS);
		}
		else if(t%4 == 2){
			ret = ret * glm::rotate(glm::mat4(1.0f), glm::radians(-10.0f), Y_AXIS);
		}else{
			ret = glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 0.7f));
		}
		return ret;
	}
	
	glm::mat4 bodyMatrix(float time) {
		int t = (int)(time*2);
		glm::mat4 ret;
		if(t%2 == 1){
			ret = glm::rotate(glm::mat4(1.0f), glm::radians(5.0f), Y_AXIS);
		}
		else{
			ret = glm::rotate(glm::mat4(1.0f), glm::radians(-5.0f), Y_AXIS);
		}
		return ret;
	}
	
public:
	
	CanadaRoom(int _maxPortalDepth=3) : StandardRoom{_maxPortalDepth} {};
	
	void drawObjectsInThisRoom(const glm::vec3& cam, const glm::mat4& worldToView, const glm::mat4& viewToProj, bool isFirstRoom) override {
		float t = glfwGetTime();
		
		// Update animations
		head->setModelToWorld(headMatrix(t));
// 		body->setModelToWorld(bodyMatrix(t));
		
		head->drawModel(cam, worldToView, viewToProj, lights);
		body->drawModel(cam, worldToView, viewToProj, lights);
		// Draw this room
		for(ObjectInstance* instance : objects)
			instance->drawModel(cam, worldToView, viewToProj, lights);
		
		
	}
	
	void setHead(ObjectInstance* h){
		head = h;
	}
	
	void setBody(ObjectInstance* b){
		body = b;
	}
};

class PendulumRoom : public StandardRoom {
	
	vector<ObjectInstance*> allObjects;
	vector<ObjectInstance*> pendulumParts;
	
	glm::mat4 pendulumMatrix(float time) {
		glm::mat4 pendulumMoveMatrix = glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 6.0f));
		glm::vec3 axis = rotate(X_AXIS, time/5.0f, Z_AXIS);
		return pendulumMoveMatrix * glm::rotate(glm::mat4(1.0f), (float)sin(time)/2, axis);
	}
	
public:	
	PendulumRoom(int _maxPortalDepth=3) : StandardRoom{_maxPortalDepth} {};
	
	
	void drawObjectsInThisRoom(const glm::vec3& cam, const glm::mat4& worldToView, const glm::mat4& viewToProj, bool isFirstRoom) override {
		float t = glfwGetTime();
		glm::mat4 pendMat = pendulumMatrix(t);
		// Update animations
		for(ObjectInstance* instance : pendulumParts)
			instance->setModelToWorld(pendMat);
		
		// Update shadow map
		glDisable(GL_STENCIL_TEST); // We don't want to stencil check when rerendering shadows
		const glm::vec3 lampPos = {0.0f, 0.0f, -3.0f};
		updateShadowLightPos(0, lights, glm::vec3(pendMat*glm::vec4(lampPos, 1.0)));
		renderLampShadowMaps(lights, 0, allObjects);
		
		postProcessor.activateHDRBuffer();
		if(!isFirstRoom) // Only reactivate stencil test if we are not in this room
			glEnable(GL_STENCIL_TEST);
		
		// Draw this room
		for(ObjectInstance* instance : objects)
			instance->drawModel(cam, worldToView, viewToProj, lights);
		
		// Draw pendulum
		for(ObjectInstance* instance : pendulumParts)
			instance->drawModel(cam, worldToView, viewToProj, lights);
	}
	
	void addPendulum(ObjectInstance* pend) {
		pendulumParts.push_back(pend);
		allObjects.push_back(pend);
	}

	void addObject(ObjectInstance* obj) override {
		Room::addObject(obj);
		allObjects.push_back(obj);
	}
};



class GalleryRoom : public StandardRoom {
	ObjectInstance* floor;
	vector<ObjectInstance*> mirrorObjects;
	LightData mirrorLights;
	
public:	
	GalleryRoom(int _maxPortalDepth=3) : StandardRoom{_maxPortalDepth} {};
	
	void addFloor(ObjectInstance* _floor) {
		floor = _floor;
	}
	
	void addObject(ObjectInstance* instance) override {
		Room::addObject(instance);
		ObjectInstance* instanceCopy = new ObjectInstance(*instance);
		instanceCopy->setModelToWorld(glm::scale(glm::vec3(1.0f, 1.0f, -1.0f))*instanceCopy->getModelToWorld());
		mirrorObjects.push_back(instanceCopy);
	}
	
	void drawObjectsInThisRoom(const glm::vec3& cam, const glm::mat4& worldToView, const glm::mat4& viewToProj, bool isFirstRoom) override {
		// Draw this room
		glDisable(GL_BLEND);
		for(ObjectInstance* instance : objects)
			instance->drawModel(cam, worldToView, viewToProj, lights);
		
		// Draw reflection room
		glFrontFace(GL_CW);
		for(ObjectInstance* instance : mirrorObjects)
			instance->drawModel(cam, worldToView, viewToProj, mirrorLights);
		
		// Draw floor using alpha
		glFrontFace(GL_CCW);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		floor->drawModel(cam, worldToView, viewToProj, lights);
	}
	
	
	void addShadowLight(glm::vec3 position, glm::vec3 color, float intensity) override {
		Room::addShadowLight(position, color, intensity);
		
		// Add shadow light in reflected room
		glm::vec3 mirrorPos(position);
		mirrorPos[2] = -mirrorPos[2];
		mirrorLights.addShadowLight(mirrorPos, color, intensity);
		renderLampShadowMaps(mirrorLights, 0, mirrorObjects); // Last shadow map always has index 0
	}
	
};

