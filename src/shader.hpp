#pragma once

#include "GL_utilities.h"


struct ShaderProgram {
	uint programId;
	const char* verticesName; // The name of the variables in the shader
	const char* normalsName;
	const char* uvsName; 
	
	ShaderProgram(uint _programId, const char* _vertName, const char* _normalName, const char* _uvsName) : 
		programId{_programId}, verticesName{_vertName}, normalsName{_normalName}, uvsName{_uvsName} {};
	~ShaderProgram() = default;
};


/**
 * A struct where all compiled shaders can be found. 
 */
struct ShaderRegistry {
	ShaderProgram* phongProgram;
	ShaderProgram* phongNormalProgram;
	ShaderProgram* phongShadowsProgram;
	ShaderProgram* phongReflectionShadowsProgram;
	ShaderProgram* lightMapProgram;
	ShaderProgram* lightEmissionProgram;
	uint portalProgramId;
	uint shadowDepthProgramId;
	uint hdrPostProcessId;
	uint blurPostProcessId;
	
	void compileShaders() {
		uint programId = loadShaders("shaders/phong_shader.vert", "shaders/phong_shader.frag");
		phongProgram = new ShaderProgram(programId, "inPosition", "inNormal", "inTexCoord");
		
		programId = loadShaders("shaders/phong_normal_shader.vert", "shaders/phong_normal_shader.frag");
		phongNormalProgram = new ShaderProgram(programId, "inPosition", "inNormal", "inTexCoord");
		
		programId = loadShaders("shaders/phong_shader_shadows.vert", "shaders/phong_shader_shadows.frag");
		phongShadowsProgram = new ShaderProgram(programId, "inPosition", "inNormal", "inTexCoord");
		
		programId = loadShaders("shaders/phong_shader_shadows.vert", "shaders/phong_shader_floor_shadows.frag");
		phongReflectionShadowsProgram = new ShaderProgram(programId, "inPosition", "inNormal", "inTexCoord");
		
		programId = loadShaders("shaders/lightmapping.vert", "shaders/lightmapping.frag");
		lightMapProgram = new ShaderProgram(programId, "inPosition", nullptr, "inTexCoord");
		
		programId = loadShaders("shaders/light_emission.vert", "shaders/light_emission.frag");
		lightEmissionProgram = new ShaderProgram(programId, "inPosition", nullptr, "inTexCoord");
		
		portalProgramId = loadShaders("shaders/portal.vert", "shaders/portal.frag");
		shadowDepthProgramId = loadShadersG("shaders/point_shadow_shader.vert", "shaders/point_shadow_shader.frag", "shaders/point_shadow_shader.geom");
		hdrPostProcessId = loadShaders("shaders/hdr_shader.vert", "shaders/hdr_shader.frag");
		blurPostProcessId = loadShaders("shaders/blur_shader.vert", "shaders/blur_shader.frag");
		
	}
};


ShaderRegistry shaderReg;

