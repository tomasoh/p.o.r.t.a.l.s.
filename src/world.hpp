#pragma once

#include <vector>
#include <glm/mat4x4.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>
#include <cmath>

#include "roomtypes.hpp"
#include "meshobject.hpp"
#include "shader.hpp"
#include "objimporter.hpp"

const glm::vec3 COLOR_FIRE = {1.0f, 0.95, 0.65};

const glm::vec3 Z_AXIS = {0.0f, 0.0f, 1.0f};
const glm::vec3 Y_AXIS = {0.0f, 1.0f, 0.0f};
const glm::vec3 X_AXIS = {1.0f, 0.0f, 0.0f};

/**
 * A class that keeps track of loaded meshes.
 * We can use this to load a mesh once, and reuse the same MeshObject
 * when we want to load it again. Instead of loading another copy of it 
 * to the GPU, we return the earlier copy.
 */
class MeshRegistry {
	std::unordered_map<string, MeshObject*> importedMeshes;
	
public:
	MeshRegistry() {};
	~MeshRegistry() = default;
	
	MeshObject* loadModel(const string& filename) {
		auto it = importedMeshes.find(filename);
		if(it != end(importedMeshes))
			return it->second;
		else {
			MeshObject* mesh = importOBJ(filename);
			mesh->uploadModelToGPU();
			importedMeshes[filename] = mesh;
			return mesh;
		}
	}
	
	CollisionObject* loadCollisionModel(const string& filename, const glm::mat4 &modelToWorld) {
		CollisionObject* mesh = importProxyOBJ(filename);
		mesh->applyTransformation(modelToWorld);
		return mesh;
	}
};


/**
 * A class that keeps track of id's to loaded texture.
 * We can use this to load a texture once, and reuse the same id
 * when we want to load it again, instead of loading another copy of it 
 * to the GPU.
 */
class TextureRegistry {
	std::unordered_map<string, uint> importedTextures;
	
public:
	TextureRegistry() {};
	~TextureRegistry() = default;
	
	uint loadTexture(const char* filename) {
		auto it = importedTextures.find(filename);
		if(it != end(importedTextures))
			return it->second;
		else {
			uint textureId;
			LoadTGATextureSimple(filename, &textureId);
			importedTextures[filename] = textureId;
			return textureId;
		}
	}
};




/**
 * A class containing the loaded meshes and textures, structured as rooms.
 * This class is responsible of creating the rooms and connecing them by 
 * portals. It also keeps track of which room we are currently inside.
 */
class World {
	std::vector<Room*> rooms;
	Room* currentRoom = nullptr;
	MeshRegistry meshReg;
	TextureRegistry texReg;
	
public:
	
	World() {};
	~World() = default;
	
	Room* createPortalRoom(){
		// "Portal" room
		MeshObject* walls = meshReg.loadModel("models/portalroom/portalroom_walls.obj");
		MeshObject* floor = meshReg.loadModel("models/portalroom/portalroom_floor.obj");
		MeshObject* archway = meshReg.loadModel("models/archway.obj");
		
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		uint wallTexture = texReg.loadTexture("models/portalroom/portal_wall_texture.tga");
		uint floorTexture = texReg.loadTexture("models/portalroom/ConcreteFloors0058.tga");
		
		Room* room = new StandardRoom(2);
		room->addLight(glm::vec3(0.0f, -2.0f, 2.3f), glm::vec3(1.0f, 1.0f, 1.0f), 7.0);
		
		ObjectInstance* wallInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.phongProgram);
		wallInstance->addTexture(wallTexture);
		room->addObject(wallInstance);
		
		ObjectInstance* floorInstance = new ObjectInstance(floor, glm::mat4(1.0), shaderReg.phongProgram);
		floorInstance->addTexture(floorTexture);
		room->addObject(floorInstance);
		
		glm::mat4 rotateMatrix = glm::rotate(-glm::radians(90.0f), Z_AXIS);
		ObjectInstance* archwayInstance = new ObjectInstance(archway, rotateMatrix, shaderReg.phongProgram);
		archwayInstance->addTexture(archTex);
		room->addObject(archwayInstance);
		
		glm::mat4 arch2Matrix = glm::translate(glm::mat4(1.0), glm::vec3(4.97f, 2.0f, 0.0f)) * rotateMatrix;
		ObjectInstance* archwayInstance2 = new ObjectInstance(archway, arch2Matrix, shaderReg.phongProgram);
		archwayInstance2->addTexture(archTex);
		room->addObject(archwayInstance2);
		
		CollisionObject* wallsCollision = meshReg.loadCollisionModel("models/portalroom/portalroom_proxy.obj", glm::mat4(1.0));
		CollisionObject* archwayCollision = meshReg.loadCollisionModel("models/archway_collision.obj", rotateMatrix);
		CollisionObject* archwayCollision2 = meshReg.loadCollisionModel("models/archway_collision.obj", arch2Matrix);
		room->addCollisionProxy(wallsCollision);
		room->addCollisionProxy(archwayCollision);
		room->addCollisionProxy(archwayCollision2);
		rooms.push_back(room);
		return room;
	}
	
	Room* createCornellBoxRoom(){
		// Cornell box room
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		uint cornellTexture = texReg.loadTexture("models/cornellbox_room/cornellbox_bake.tga");
		Room* room = new StandardRoom();
		
		MeshObject* walls = meshReg.loadModel("models/cornellbox_room/cornellbox_room.obj");
		MeshObject* archway = meshReg.loadModel("models/archway.obj");
		
		glm::mat4 m = glm::mat4(1.0);
		m = glm::translate(m, glm::vec3(0.0, -3.24, 0.0));
		
		ObjectInstance* archwayInstance = new ObjectInstance(archway, m, shaderReg.lightMapProgram);
		archwayInstance->addTexture(archTex);
		room->addObject(archwayInstance);
		
		ObjectInstance* wallInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.lightMapProgram);
		wallInstance->addTexture(cornellTexture);
		room->addObject(wallInstance);
		
		CollisionObject* wallsCollision = meshReg.loadCollisionModel("models/cornellbox_room/cornellbox_room_proxy.obj", glm::mat4(1.0));
		CollisionObject* archwayCollision = meshReg.loadCollisionModel("models/archway_collision.obj", m);
		room->addCollisionProxy(wallsCollision);
		room->addCollisionProxy(archwayCollision);
		rooms.push_back(room);
		return room;
	}
	
	// Create a instance of a room for the cornidoor room. 
	Room* createCornidoorRoomInstance(const vector<ObjectInstance*>& objects, CollisionObject* proxy) {
		Room* room = new StandardRoom(2);
		rooms.push_back(room);
		for(ObjectInstance* obj : objects)
			room->addObject(obj);

		room->addCollisionProxy(proxy);
		room->addLight(glm::vec3(1.2f, -3.5f, 2.3f), COLOR_FIRE, 5.8f);
		room->addLight(glm::vec3(3.5f, -1.2f, 2.3f), COLOR_FIRE, 5.8f);
		// Add extra lights outside this "room"" to simulate light from next "room"
		room->addLight(glm::vec3(1.2f, -9.5f, 2.3f), COLOR_FIRE, 5.8f);
		room->addLight(glm::vec3(9.5f, -1.2f, 2.3f), COLOR_FIRE, 5.8f);
		return room;
	}
	
	Room* createCornidoor(Room* infinityRoom, Portal* infinityPortal){
		uint wallTexture = texReg.loadTexture("models/cornidoor/brickwall.tga");
		uint wallTextureNormal = texReg.loadTexture("models/cornidoor/brickwall_normal.tga");
		uint floorTexture = texReg.loadTexture("models/cornidoor/concrete02.tga");
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		uint crateTexture = texReg.loadTexture("models/container2.tga");
		uint barrelTexture = texReg.loadTexture("models/barrel_diffuse.tga");
		uint blackTexture = texReg.loadTexture("models/simple_corridor/plain_black.tga");
		uint lampTexture = texReg.loadTexture("models/light_texture.tga");
		
		// Load models
		MeshObject* portalMesh = meshReg.loadModel("models/portal.obj");
		
		vector<ObjectInstance*> commonObjects;
		MeshObject* walls = meshReg.loadModel("models/cornidoor/wall_mesh.obj");
		ObjectInstance* wallsInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.phongNormalProgram);
		wallsInstance->addTexture(wallTexture);
		wallsInstance->addTexture(wallTextureNormal);
		wallsInstance->materialSpecularity = 0.6;
		wallsInstance->materialDiffuse = 0.4;
		wallsInstance->materialAmbient= 0.05;
		commonObjects.push_back(wallsInstance);
		
		MeshObject* floors = meshReg.loadModel("models/cornidoor/ground_mesh.obj");
		ObjectInstance* floorsInstance = new ObjectInstance(floors, glm::mat4(1.0), shaderReg.phongProgram);
		floorsInstance->addTexture(floorTexture);
		floorsInstance->materialDiffuse = 0.4;
		floorsInstance->materialAmbient= 0.05;
		floorsInstance->materialSpecularity = 0.1;
		commonObjects.push_back(floorsInstance);
		
		MeshObject* archway = meshReg.loadModel("models/archway.obj");
		const glm::mat4 archwayMatrix = rotate(glm::radians(45.0f), Z_AXIS);
		ObjectInstance* archwayInstance = new ObjectInstance(archway, archwayMatrix, shaderReg.phongProgram);
		archwayInstance->addTexture(archTex);
		
		glm::mat4 lampMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, -3.5f, 2.3f))*glm::rotate(-glm::radians(90.0f), Z_AXIS);
		glm::mat4 lampMatrix2 = glm::translate(glm::mat4(1.0f), glm::vec3(3.5f, -1.5f, 2.3f))*glm::rotate(glm::radians(180.0f), Z_AXIS);
		MeshObject* lampHouse = meshReg.loadModel("models/cornidoor/lamp_house.obj");
		MeshObject* lampLight = meshReg.loadModel("models/cornidoor/lamp_light.obj");
		ObjectInstance* lampHouseInstance = new ObjectInstance(lampHouse, lampMatrix, shaderReg.phongProgram);
		ObjectInstance* lampLightInstance = new ObjectInstance(lampLight, lampMatrix, shaderReg.lightEmissionProgram);
		ObjectInstance* lampHouseInstance2 = new ObjectInstance(lampHouse, lampMatrix2, shaderReg.phongProgram);
		ObjectInstance* lampLightInstance2 = new ObjectInstance(lampLight, lampMatrix2, shaderReg.lightEmissionProgram);
		lampHouseInstance->addTexture(blackTexture);
		lampLightInstance->addTexture(lampTexture);
		lampHouseInstance2->addTexture(blackTexture);
		lampLightInstance2->addTexture(lampTexture);
		commonObjects.push_back(lampHouseInstance);
		commonObjects.push_back(lampLightInstance);
		commonObjects.push_back(lampHouseInstance2);
		commonObjects.push_back(lampLightInstance2);
		
		MeshObject* cornidoorPortalMesh = meshReg.loadModel("models/cornidoor/portal_mesh.obj");
		
		MeshObject* barrelMesh = meshReg.loadModel("models/barrel.obj");
		MeshObject* crateMesh = meshReg.loadModel("models/crate.obj");
		
		//Load Collision meshes
		CollisionObject* wallProxy = meshReg.loadCollisionModel("models/cornidoor/wall_mesh_proxy.obj", glm::mat4(1.0));
		CollisionObject* archwayProxy = meshReg.loadCollisionModel("models/archway_collision.obj", archwayMatrix);
		
		// Set up the rooms
		Room* startRoom = createCornidoorRoomInstance(commonObjects, wallProxy);
		startRoom->addObject(archwayInstance);
		startRoom->addCollisionProxy(archwayProxy);
		
		glm::mat4 turnMatrix = glm::rotate(glm::radians(180.0f), Z_AXIS);
		glm::mat4 portal1MtW = glm::translate(glm::vec3(0.0f, -6.5f, 0.0f)) * turnMatrix;
		glm::mat4 portal2MtW = glm::rotate(glm::radians(90.0f), Z_AXIS) * portal1MtW;
		
		Room* prevRoom = startRoom;
		for(int i = 0; i < 8; ++i){
			
			Room* nextRoom = createCornidoorRoomInstance(commonObjects, wallProxy);
			Portal* inPortal1 = new Portal(cornidoorPortalMesh, portal1MtW, true, shaderReg.portalProgramId);
			Portal* inPortal2 = new Portal(cornidoorPortalMesh, portal2MtW, true, shaderReg.portalProgramId);
			
			inPortal1->setToPortal(prevRoom, inPortal2);
			inPortal2->setToPortal(nextRoom, inPortal1);
			
			nextRoom->addPortal(inPortal1);
			prevRoom->addPortal(inPortal2);
			
			if(i == 6) {
				prevRoom->addObject(archwayInstance);
				prevRoom->addCollisionProxy(archwayProxy);
				Portal* toInfPortal = new Portal(portalMesh, archwayMatrix, true, shaderReg.portalProgramId);
				prevRoom->addPortal(toInfPortal);
				toInfPortal->setToPortal(infinityRoom, infinityPortal);
				infinityPortal->setToPortal(prevRoom, toInfPortal);
				
			}
			
			if(i == 1 or i == 3 or i == 5){
				glm::mat4 barrelMatrix = glm::translate(glm::mat4(1.0), glm::vec3(-1.0, 1.0, 0.0));
				ObjectInstance* barrelInstance = new ObjectInstance(barrelMesh, barrelMatrix, shaderReg.phongProgram);
				barrelInstance->addTexture(barrelTexture);
				nextRoom->addObject(barrelInstance);
			}
			
			if(i == 2 or i == 5){
				glm::mat4 crateMatrix = glm::translate(glm::mat4(1.0), glm::vec3(-1.0, 1.0, 0.0));
				ObjectInstance* crateInstance = new ObjectInstance(crateMesh, crateMatrix, shaderReg.phongProgram);
				crateInstance->addTexture(crateTexture);
				nextRoom->addObject(crateInstance);
			}
			
			prevRoom = nextRoom;
			
		}
		
		Portal* inPortal1 = new Portal(cornidoorPortalMesh, portal1MtW, true, shaderReg.portalProgramId);
		Portal* inPortal2 = new Portal(cornidoorPortalMesh, portal2MtW, true, shaderReg.portalProgramId);
		
		inPortal1->setToPortal(prevRoom, inPortal2);
		inPortal2->setToPortal(startRoom, inPortal1);
		
		startRoom->addPortal(inPortal1);
		prevRoom->addPortal(inPortal2);
		
		return startRoom;
	}
	
	
	InfinityRoom* createInfinityCorridorRoom() {
		MeshObject* walls = meshReg.loadModel("models/infinity_corridor/infinity_corridor_walls.obj");
		MeshObject* floor = meshReg.loadModel("models/infinity_corridor/infinity_corridor_floor.obj");
		MeshObject* ceiling = meshReg.loadModel("models/infinity_corridor/infinity_corridor_ceiling.obj");
		MeshObject* archway = meshReg.loadModel("models/archway.obj");
		MeshObject* portalMesh = meshReg.loadModel("models/cornidoor/portal_mesh.obj");
		MeshObject* lampHandle = meshReg.loadModel("models/infinity_corridor/lamp_handle.obj");
		MeshObject* lampLight = meshReg.loadModel("models/infinity_corridor/lamp_light.obj");
		
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		uint wallTexture = texReg.loadTexture("models/infinity_corridor/wall_wood.tga");
		uint floorTexture = texReg.loadTexture("models/infinity_corridor/rug.tga");
		uint ceilingTexture = texReg.loadTexture("models/infinity_corridor/ceiling_white.tga");
		uint lampTexture = texReg.loadTexture("models/infinity_corridor/lamp_baked.tga");
		uint lightTexture = texReg.loadTexture("models/light_texture.tga");
		
		InfinityRoom* room = new InfinityRoom();
		// Lights on walls
		room->addLight(glm::vec3(1.05f, -3.75f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		room->addLight(glm::vec3(-1.2f, -3.7f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		room->addLight(glm::vec3(1.05f, 3.75f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		room->addLight(glm::vec3(-1.2f, 3.75f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		
		ObjectInstance* lampHandle0 = new ObjectInstance(lampHandle, glm::translate(glm::mat4(1.0f), glm::vec3(-1.36f, -3.75f, 1.5f)), shaderReg.lightMapProgram);
		ObjectInstance* lampHandle1 = new ObjectInstance(lampHandle, glm::translate(glm::rotate(glm::radians(180.0f), Z_AXIS), glm::vec3(-1.23f, -3.75f, 1.5f)), shaderReg.lightMapProgram);
		ObjectInstance* lampHandle2 = new ObjectInstance(lampHandle, glm::translate(glm::mat4(1.0f), glm::vec3(-1.36f, 3.75f, 1.5f)), shaderReg.lightMapProgram);
		ObjectInstance* lampHandle3 = new ObjectInstance(lampHandle, glm::translate(glm::rotate(glm::radians(180.0f), Z_AXIS), glm::vec3(-1.23f, 3.75f, 1.5f)), shaderReg.lightMapProgram);
		for(ObjectInstance* lamp : {lampHandle0, lampHandle1, lampHandle2, lampHandle3}) {
			room->addObject(lamp);
			lamp->addTexture(lampTexture);
		}
		ObjectInstance* lampLight0 = new ObjectInstance(lampLight, glm::translate(glm::mat4(1.0f), glm::vec3(-1.36f, -3.75f, 1.5f)), shaderReg.lightEmissionProgram);
		ObjectInstance* lampLight1 = new ObjectInstance(lampLight, glm::translate(glm::rotate(glm::radians(180.0f), Z_AXIS), glm::vec3(-1.23f, -3.75f, 1.5f)), shaderReg.lightEmissionProgram);
		ObjectInstance* lampLight2 = new ObjectInstance(lampLight, glm::translate(glm::mat4(1.0f), glm::vec3(-1.36f, 3.75f, 1.5f)), shaderReg.lightEmissionProgram);
		ObjectInstance* lampLight3 = new ObjectInstance(lampLight, glm::translate(glm::rotate(glm::radians(180.0f), Z_AXIS), glm::vec3(-1.23f, 3.75f, 1.5f)), shaderReg.lightEmissionProgram);
		for(ObjectInstance* lamp : {lampLight0, lampLight1, lampLight2, lampLight3}) {
			room->addObject(lamp);
			lamp->addTexture(lightTexture);
		}
		
		// Lights "outside" of the room to simulate that the corridor continues
		room->addLight(glm::vec3(1.0f, -8.25f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		room->addLight(glm::vec3(-1.0f, -8.25f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		room->addLight(glm::vec3(1.0f, 8.25f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		room->addLight(glm::vec3(-1.0f, 8.25f, 1.5f), glm::vec3(1.0f, 1.0f, 1.0f), 0.4);
		
		ObjectInstance* wallInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.phongProgram);
		wallInstance->addTexture(wallTexture);
		wallInstance->materialDiffuse = 0.5f;
		wallInstance->materialSpecularity = 0.8f;
		wallInstance->materialSpecularHardness = 64.0f;
		wallInstance->materialAmbient = 0.05f;
		room->addObject(wallInstance);
		
		ObjectInstance* floorInstance = new ObjectInstance(floor, glm::mat4(1.0), shaderReg.phongProgram);
		floorInstance->addTexture(floorTexture);
		floorInstance->materialDiffuse = 0.7f;
		floorInstance->materialSpecularity = 0.5f;
		floorInstance->materialAmbient = 0.05f;
		room->addObject(floorInstance);
		
		ObjectInstance* ceilingInstance = new ObjectInstance(ceiling, glm::mat4(1.0), shaderReg.phongProgram);
		ceilingInstance->addTexture(ceilingTexture);
		ceilingInstance->materialSpecularity = 0.1f;
		ceilingInstance->materialDiffuse = 0.5f;
		room->addObject(ceilingInstance);
		
		glm::mat4 archwayMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(2.97, 0.0f, 0.0f))*glm::rotate(-glm::radians(90.0f), Z_AXIS);
		ObjectInstance* archwayInstance = new ObjectInstance(archway, archwayMatrix, shaderReg.phongProgram);
		archwayInstance->addTexture(archTex);
		room->addObject(archwayInstance);
		
		glm::mat4 portalLeftMat = glm::translate(glm::rotate(glm::radians(180.0f), Z_AXIS), glm::vec3(0.0f, 6.0f, 0.0f));
		glm::mat4 portalRightMat = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 6.0f, 0.0f));
		Portal* portalLeft = new Portal(portalMesh, portalLeftMat, true, shaderReg.portalProgramId);
		Portal* portalRight = new Portal(portalMesh, portalRightMat, true, shaderReg.portalProgramId);
		portalLeft->setToPortal(room, portalRight);
		portalRight->setToPortal(room, portalLeft);
		room->addLoopingLeftPortal(portalLeft);
		room->addLoopingRightPortal(portalRight);
		
		CollisionObject* wallsCollision = meshReg.loadCollisionModel("models/infinity_corridor/infinity_corridor_proxy.obj", glm::mat4(1.0));
		CollisionObject* archwayCollision = meshReg.loadCollisionModel("models/archway_collision.obj", archwayMatrix);
		room->addCollisionProxy(wallsCollision);
		room->addCollisionProxy(archwayCollision);
		rooms.push_back(room);
		return room;
	}
	
	
	Room* createGalleryRoom() {
		MeshObject* walls = meshReg.loadModel("models/gallery/gallery_walls.obj");
		MeshObject* paintings = meshReg.loadModel("models/gallery/gallery_paintings.obj");
		MeshObject* floor = meshReg.loadModel("models/gallery/gallery_floor.obj");
		MeshObject* ceiling = meshReg.loadModel("models/gallery/gallery_ceiling.obj");
		MeshObject* archway = meshReg.loadModel("models/archway.obj");
		
		MeshObject* column = meshReg.loadModel("models/gallery/column.obj");
		MeshObject* suzanne = meshReg.loadModel("models/gallery/suzanne.obj");
		MeshObject* stanfordBunny = meshReg.loadModel("models/gallery/stanford_bunny.obj");
		MeshObject* utahTeapot = meshReg.loadModel("models/gallery/utah_teapot.obj");
		
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		uint wallTexture = texReg.loadTexture("models/infinity_corridor/ceiling_white.tga");
		uint floorTexture = texReg.loadTexture("models/gallery/marble.tga");
		uint floorSpecTexture = texReg.loadTexture("models/gallery/marble_specmap.tga");
		uint ceilingTexture = texReg.loadTexture("models/infinity_corridor/ceiling_white.tga");
		uint paintingTexture = texReg.loadTexture("models/gallery/paintings.tga");
		uint graniteTexture = texReg.loadTexture("models/gallery/Granite_Dark.tga");
		
		GalleryRoom* room = new GalleryRoom();
		
		ObjectInstance* wallInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.phongShadowsProgram);
		wallInstance->addTexture(wallTexture);
		room->addObject(wallInstance);

		ObjectInstance* paintingInstance = new ObjectInstance(paintings, glm::mat4(1.0), shaderReg.phongShadowsProgram);
		paintingInstance->addTexture(paintingTexture);
		room->addObject(paintingInstance);
		
		ObjectInstance* floorInstance = new ObjectInstance(floor, glm::mat4(1.0), shaderReg.phongReflectionShadowsProgram);
		floorInstance->addTexture(floorTexture);
		floorInstance->addTexture(floorSpecTexture);
		room->addFloor(floorInstance);
		
		ObjectInstance* ceilingInstance = new ObjectInstance(ceiling, glm::mat4(1.0), shaderReg.phongShadowsProgram);
		ceilingInstance->addTexture(ceilingTexture);
		room->addObject(ceilingInstance);
		
		vector<ObjectInstance*> statues;
		// Add Suzanne statue
		statues.push_back(new ObjectInstance(column, glm::translate(glm::mat4(1.0f), glm::vec3(-2.5, 2.5, 0.0)), shaderReg.phongShadowsProgram));
		statues.push_back(new ObjectInstance(suzanne, glm::translate(glm::mat4(1.0f), glm::vec3(-2.5, 2.5, 1.5)), shaderReg.phongShadowsProgram));
		// Add Stanford Bunny statue
		statues.push_back(new ObjectInstance(column, glm::translate(glm::mat4(1.0f), glm::vec3(4.0, -3.0, 0.0)), shaderReg.phongShadowsProgram));
		statues.push_back(new ObjectInstance(stanfordBunny, glm::translate(glm::mat4(1.0f), glm::vec3(4.0, -3.0, 1.5)), shaderReg.phongShadowsProgram));
		// Add Utah Teapot statue
		statues.push_back(new ObjectInstance(column, glm::translate(glm::mat4(1.0f), glm::vec3(0.0, -2.0, 0.0)), shaderReg.phongShadowsProgram));
		statues.push_back(new ObjectInstance(utahTeapot, glm::translate(glm::mat4(1.0f), glm::vec3(0.0, -2.0, 1.5)), shaderReg.phongShadowsProgram));
		
		for(ObjectInstance* obj : statues) {
			obj->addTexture(graniteTexture);
			room->addObject(obj);
		}
		
		glm::mat4 archwayMatrix = glm::rotate(glm::radians(90.0f), Z_AXIS);
		ObjectInstance* archwayInstance = new ObjectInstance(archway, archwayMatrix, shaderReg.phongShadowsProgram);
		archwayInstance->addTexture(archTex);
		room->addObject(archwayInstance);
		
		glm::mat4 archwayMatrix2 = glm::translate(archwayMatrix, glm::vec3(0.0, 2.61, 0.0));
		ObjectInstance* archwayInstance2 = new ObjectInstance(archway, archwayMatrix2, shaderReg.phongShadowsProgram);
		archwayInstance2->addTexture(archTex);
		room->addObject(archwayInstance2);
		
		CollisionObject* wallsCollision = meshReg.loadCollisionModel("models/gallery/gallery_proxy.obj", glm::mat4(1.0));
		CollisionObject* archwayCollision = meshReg.loadCollisionModel("models/archway_collision.obj", archwayMatrix);
		CollisionObject* archwayCollision2 = meshReg.loadCollisionModel("models/archway_collision.obj", archwayMatrix2);
		room->addCollisionProxy(wallsCollision);
		room->addCollisionProxy(archwayCollision);
		room->addCollisionProxy(archwayCollision2);
		room->addCollisionProxy(meshReg.loadCollisionModel("models/gallery/statue_proxy.obj", glm::translate(glm::mat4(1.0f), glm::vec3(-2.5, 2.5, 0.0))));
		room->addCollisionProxy(meshReg.loadCollisionModel("models/gallery/statue_proxy.obj", glm::translate(glm::mat4(1.0f), glm::vec3(4.0, -3.0, 0.0))));
		room->addCollisionProxy(meshReg.loadCollisionModel("models/gallery/statue_proxy.obj", glm::translate(glm::mat4(1.0f), glm::vec3(0.0, -2.0, 0.0))));
		
		// Add a shadow light and render its depth texture
		room->addShadowLight(glm::vec3(-2.0, -2.0, 1.8), glm::vec3(1.0, 1.3, 1.0), 3.0);
		rooms.push_back(room);
		return room;
	}
	
	
	Room* createPendulumRoom() {
		MeshObject* walls = meshReg.loadModel("models/portalroom/portalroom_walls.obj");
		MeshObject* floor = meshReg.loadModel("models/portalroom/portalroom_floor.obj");
		MeshObject* pendulum = meshReg.loadModel("models/pendulum/pendulum_crown.obj");
		MeshObject* pendulumLamp = meshReg.loadModel("models/pendulum/pendulum_lamp.obj");
		MeshObject* barrelMesh = meshReg.loadModel("models/barrel.obj");
		MeshObject* crateMesh = meshReg.loadModel("models/crate.obj");
		MeshObject* companionCube = meshReg.loadModel("models/companion_cube.obj");
		MeshObject* archway = meshReg.loadModel("models/archway.obj");
		
		uint wallTexture = texReg.loadTexture("models/portalroom/portal_wall_texture.tga");
		uint floorTexture = texReg.loadTexture("models/portalroom/ConcreteFloors0058.tga");
		uint pendulumTexture = texReg.loadTexture("models/infinity_corridor/wall_wood.tga");
		uint barrelTexture = texReg.loadTexture("models/barrel_diffuse.tga");
		uint crateTexture = texReg.loadTexture("models/container2.tga");
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		uint lightTexture = texReg.loadTexture("models/light_texture.tga");
		uint compainonTexture = texReg.loadTexture("models/companion_cube_diffuse.tga");
		
		PendulumRoom* room = new PendulumRoom(2);
		
		ObjectInstance* wallInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.phongShadowsProgram);
		wallInstance->addTexture(wallTexture);
		wallInstance->materialAmbient = 1.4;
		room->addObject(wallInstance);
		
		ObjectInstance* floorInstance = new ObjectInstance(floor, glm::mat4(1.0), shaderReg.phongShadowsProgram);
		floorInstance->addTexture(floorTexture);
		floorInstance->materialAmbient = 1.4;
		room->addObject(floorInstance);
		
		ObjectInstance* pendulumInstance = new ObjectInstance(pendulum, glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 6.0f)), shaderReg.phongProgram);
		ObjectInstance* pendulumLampInstance = new ObjectInstance(pendulumLamp, glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, 6.0f)), shaderReg.lightEmissionProgram);
		pendulumInstance->materialDiffuse = 0.2;
		pendulumInstance->materialSpecularity = 0.1;
		pendulumInstance->addTexture(pendulumTexture);
		pendulumLampInstance->addTexture(lightTexture);
		room->addPendulum(pendulumInstance);
		room->addPendulum(pendulumLampInstance);
		
		
		for(glm::vec3 pos : vector<glm::vec3>{{-1.0f, 1.0f, 0.0f}, {-0.5f, 1.3f, 0.0f}, {-2.2f, 2.4f, 0.0f}, {-2.8f, 2.3f, 0.0f}, {-2.6f, 2.9f, 0.0f}, {-2.5f, 2.5f, 0.82f}}) {
			ObjectInstance* barrelInstance = new ObjectInstance(barrelMesh, glm::translate(glm::mat4(1.0), pos), shaderReg.phongShadowsProgram);
			barrelInstance->addTexture(barrelTexture);
			barrelInstance->materialAmbient = 1.4;
			room->addObject(barrelInstance);
		}
		
		for(glm::vec3 pos : vector<glm::vec3>{{2.0f, 2.0f, 0.0f}, {2.7f, 3.1f, 0.0f}, {2.3f, 2.6f, 1.0f}}) {
			ObjectInstance* crateIns = new ObjectInstance(crateMesh, glm::translate(glm::mat4(1.0f), pos), shaderReg.phongShadowsProgram);
			crateIns->addTexture(crateTexture);
			crateIns->materialAmbient = 1.4;
			room->addObject(crateIns);
		}
		
		glm::vec3 pos{2.0f, -3.8f, 2.0f};
		ObjectInstance* conpanionIns = new ObjectInstance(companionCube, glm::translate(glm::mat4(1.0f), pos), shaderReg.phongShadowsProgram);
		conpanionIns->addTexture(compainonTexture);
		conpanionIns->materialAmbient = 1.4;
		room->addObject(conpanionIns);
		
		room->addShadowLight(glm::vec3(0.0f, -2.0f, 2.3f), glm::vec3(1.0f, 1.0f, 1.0f), 4.0);
		
		glm::mat4 rotateMatrix = glm::rotate(-glm::radians(90.0f), Z_AXIS);
		glm::mat4 archMatrix = glm::translate(glm::mat4(1.0), glm::vec3(4.97f, 2.0f, 0.0f)) * rotateMatrix;
		ObjectInstance* archwayInstance = new ObjectInstance(archway, archMatrix, shaderReg.phongProgram);
		archwayInstance->addTexture(archTex);
		room->addObject(archwayInstance);
		
		CollisionObject* archCollision = meshReg.loadCollisionModel("models/archway_collision.obj", archMatrix);
		CollisionObject* wallsCollision = meshReg.loadCollisionModel("models/portalroom/portalroom_proxy.obj", glm::mat4(1.0));
		room->addCollisionProxy(wallsCollision);
		room->addCollisionProxy(archCollision);
		rooms.push_back(room);
		return room;
	}
	
	
	Room* createCanadaRoom(){
		MeshObject* walls = meshReg.loadModel("models/basic_room.obj");
		MeshObject* android_head = meshReg.loadModel("models/canada/android_head.obj");
		MeshObject* android_body = meshReg.loadModel("models/canada/android_body.obj");
		MeshObject* archway = meshReg.loadModel("models/archway.obj");
		
		uint wallTexture = texReg.loadTexture("models/canada/flag.tga");
		uint androidTexture = texReg.loadTexture("models/canada/android.tga");
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		
		CanadaRoom* room = new CanadaRoom();
		
		ObjectInstance* wallInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.phongProgram);
		wallInstance->addTexture(wallTexture);
		wallInstance->materialAmbient = 0.9;
		room->addObject(wallInstance);
		
		ObjectInstance* headInstance = new ObjectInstance(android_head, glm::mat4(1.0), shaderReg.phongProgram);
		headInstance->addTexture(androidTexture);
		headInstance->materialAmbient = 0.9;
		room->setHead(headInstance);
		
		ObjectInstance* bodyInstance = new ObjectInstance(android_body, glm::mat4(1.0), shaderReg.phongProgram);
		bodyInstance->addTexture(androidTexture);
		bodyInstance->materialAmbient = 0.9;
		room->setBody(bodyInstance);
		room->addLight(glm::vec3(2.0f, -2.0f, 2.3f), glm::vec3(1.0f, 1.0f, 1.0f), 7.0);
		
		glm::mat4 archMatrix = glm::translate(glm::rotate(glm::radians(180.0f), Z_AXIS), glm::vec3(0.0f, 3.0f, 0.0f));
		ObjectInstance* archwayInstance = new ObjectInstance(archway, archMatrix, shaderReg.phongProgram);
		archwayInstance->addTexture(archTex);
		room->addObject(archwayInstance);
		CollisionObject* archwayCollision = meshReg.loadCollisionModel("models/archway_collision.obj", archMatrix);
		room->addCollisionProxy(archwayCollision);
		
		room->addCollisionProxy(meshReg.loadCollisionModel("models/basic_room.obj", glm::mat4(1.0f)));
		
		rooms.push_back(room);
		return room;
	}
	
	
	SimpleCorridorRoom* createSimpleCorridor() {
		MeshObject* walls = meshReg.loadModel("models/simple_corridor/simple_corridor.obj");
		MeshObject* floor = meshReg.loadModel("models/simple_corridor/simple_corridor_floor.obj");
		MeshObject* lampHandle = meshReg.loadModel("models/simple_corridor/lamp_handle.obj");
		MeshObject* lampLight = meshReg.loadModel("models/simple_corridor/lamp_light.obj");
		MeshObject* archway = meshReg.loadModel("models/archway.obj");

		uint wallTexture = texReg.loadTexture("models/simple_corridor/masonry-wall-texture.tga");
		uint wallNormalTexture = texReg.loadTexture("models/simple_corridor/masonry-wall-normal-map.tga");
		uint floorTexture = texReg.loadTexture("models/cornidoor/concrete02.tga");
		uint blackTexture = texReg.loadTexture("models/simple_corridor/plain_black.tga");
		uint archTex = texReg.loadTexture("models/archway_baked.tga");
		uint lightTexture = texReg.loadTexture("models/light_texture.tga");
		
		SimpleCorridorRoom* room = new SimpleCorridorRoom(2);
		
		ObjectInstance* wallInstance = new ObjectInstance(walls, glm::mat4(1.0), shaderReg.phongNormalProgram);
		wallInstance->addTexture(wallTexture);
		wallInstance->addTexture(wallNormalTexture);
		wallInstance->materialSpecularity = 0.0;
		wallInstance->materialDiffuse = 0.4;
		wallInstance->materialSpecularHardness = 64.0;
		room->addObject(wallInstance);
		
		ObjectInstance* floorInstance = new ObjectInstance(floor, glm::mat4(1.0), shaderReg.phongProgram);
		floorInstance->addTexture(floorTexture);
		room->addObject(floorInstance);
		
		glm::mat4 lampMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, 0.0f, 1.9f))*glm::rotate(glm::radians(90.0f), Z_AXIS);
		ObjectInstance* lampHandleInstance = new ObjectInstance(lampHandle, lampMatrix, shaderReg.phongProgram);
		lampHandleInstance->addTexture(blackTexture);
		room->addObject(lampHandleInstance);
		ObjectInstance* lampLightInstance = new ObjectInstance(lampLight, lampMatrix, shaderReg.lightEmissionProgram);
		lampLightInstance->addTexture(lightTexture);
		lampLightInstance->materialLightIntensity = 1.5;
		room->addObject(lampLightInstance);
		
		glm::mat4 rotateMatrix = glm::rotate(glm::radians(90.0f), Z_AXIS);
		
		// Add all the archways
		for(const glm::mat4& mat : {glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -6.9f, 0.0f)), glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 6.9f, 0.0f)), glm::translate(glm::mat4(1.0f), glm::vec3(4.45f, 2.8f, 0.0f))*rotateMatrix, 
						glm::translate(glm::mat4(1.0f), glm::vec3(-4.45f, -2.8f, 0.0f))*rotateMatrix })  {
			
			ObjectInstance* archwayInstance = new ObjectInstance(archway, mat, shaderReg.phongProgram);
			archwayInstance->addTexture(archTex);
			room->addObject(archwayInstance);
			CollisionObject* archwayCollision = meshReg.loadCollisionModel("models/archway_collision.obj", mat);
			room->addCollisionProxy(archwayCollision);
		}
		
		room->addLight(glm::vec3(-1.25f, 0.0f, 1.9f), glm::vec3(0.8f, 0.8f, 1.0f), 3.0);
		
		CollisionObject* wallsCollision = meshReg.loadCollisionModel("models/simple_corridor/simple_corridor_proxy.obj", glm::mat4(1.0));
		room->addCollisionProxy(wallsCollision);
		
		rooms.push_back(room);
		return room;
	}
	
	void initRooms() {
		MeshObject* portalMesh = meshReg.loadModel("models/portal.obj");
		
		glm::mat4 portalRotationMatrix = glm::rotate(glm::radians(180.0f), Z_AXIS);
		glm::mat4 m = glm::translate(glm::mat4(1.0), glm::vec3(0.0, -3.24, 0.0));
		
		// Portal room
		Room* portalRoom = createPortalRoom();
		Portal* portal1 = new Portal(portalMesh, glm::rotate(-glm::radians(90.0f), Z_AXIS), true, shaderReg.portalProgramId);
		portalRoom->addPortal(portal1);
		
		glm::mat4 transMatrix = glm::translate(glm::mat4(1.0), glm::vec3(4.97f, 2.0f, 0.0f));
		Portal* portal2 = new Portal(portalMesh, transMatrix*glm::rotate(-glm::radians(90.0f), Z_AXIS), true, shaderReg.portalProgramId);
		portalRoom->addPortal(portal2);
		
		// Cornell box room
		Room* cornellBoxRoom = createCornellBoxRoom();
		Portal* portalCornell = new Portal(portalMesh, m * portalRotationMatrix, true, shaderReg.portalProgramId);
		cornellBoxRoom->addPortal(portalCornell);

		// Infinity corridor
		InfinityRoom* infinityCorridor = createInfinityCorridorRoom();
		glm::mat4 infinityPortalMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(2.97, 0.0f, 0.0f))*glm::rotate(-glm::radians(90.0f), Z_AXIS);
		Portal* infinityPortal = new Portal(portalMesh, infinityPortalMatrix, true, shaderReg.portalProgramId);
		infinityCorridor->addOuterPortal(infinityPortal);
		
		// Cornidoor room
		Room* cornidoorRoom = createCornidoor(infinityCorridor, infinityPortal);
		glm::mat4 cornidoorRotate = rotate(glm::radians(-135.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		Portal* portalCornidoor = new Portal(portalMesh, portalRotationMatrix*cornidoorRotate, true, shaderReg.portalProgramId);
		cornidoorRoom->addPortal(portalCornidoor);
		
		
		// Gallery room
		Room* galleryRoom = createGalleryRoom();
		glm::mat4 galleryMatrix = glm::rotate(glm::radians(90.0f), Z_AXIS);
		Portal* portalGallery = new Portal(portalMesh, galleryMatrix, true, shaderReg.portalProgramId);
		glm::mat4 galleryMatrix2 = glm::translate(galleryMatrix*portalRotationMatrix, glm::vec3(0.0, -2.61, 0.0));
		Portal* portalGallery2 = new Portal(portalMesh, galleryMatrix2, true, shaderReg.portalProgramId);
		galleryRoom->addPortal(portalGallery);
		galleryRoom->addPortal(portalGallery2);
		
		// Pendulum room
		Room* pendulumRoom = createPendulumRoom();
		Portal* portalPendulum = new Portal(portalMesh, transMatrix*glm::rotate(-glm::radians(90.0f), Z_AXIS), true, shaderReg.portalProgramId);
		pendulumRoom->addPortal(portalPendulum);

		// Canada room
		Room* canadaRoom = createCanadaRoom();
		Portal* portalCanada = new Portal(portalMesh, glm::translate(glm::rotate(glm::radians(180.0f), Z_AXIS), glm::vec3(0.0f, 3.0f, 0.0f)), true, shaderReg.portalProgramId);
		canadaRoom->addPortal(portalCanada);
		
		// Simple corridor room
		SimpleCorridorRoom* simpleCorridorRoom = createSimpleCorridor();
		Portal* portalSimpleCorGallery = new Portal(portalMesh, glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 6.9f, 0.0f)), true, shaderReg.portalProgramId);
		simpleCorridorRoom->addEndPointPortal(portalSimpleCorGallery);
		Portal* portalSimpleCorStartRoom = new Portal(portalMesh, glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -6.9f, 0.0f))*portalRotationMatrix, true, shaderReg.portalProgramId);
		simpleCorridorRoom->addEndPointPortal(portalSimpleCorStartRoom);
		
		Portal* portalSimpleCorCanada = new Portal(portalMesh, glm::translate(glm::mat4(1.0f), glm::vec3(-4.45f, -2.8f, 0.0f))*glm::rotate(glm::radians(90.0f), Z_AXIS), true, shaderReg.portalProgramId);
		simpleCorridorRoom->addHiddenPortal(portalSimpleCorCanada);
		Portal* portalSimpleCorPendulum = new Portal(portalMesh, glm::translate(glm::mat4(1.0f), glm::vec3(4.45f, 2.8f, 0.0f))*glm::rotate(-glm::radians(90.0f), Z_AXIS), true, shaderReg.portalProgramId);
		simpleCorridorRoom->addHiddenPortal(portalSimpleCorPendulum);
		
		
		portal1->setToPortal(cornellBoxRoom, portalCornell);
		portalCornell->setToPortal(portalRoom, portal1);
		
		portalCornidoor->setToPortal(galleryRoom, portalGallery);
		portalGallery->setToPortal(cornidoorRoom, portalCornidoor);
		
		portal2->setToPortal(simpleCorridorRoom, portalSimpleCorStartRoom);
		portalSimpleCorStartRoom->setToPortal(portalRoom, portal2);
		
		portalSimpleCorGallery->setToPortal(galleryRoom, portalGallery2);
		portalGallery2->setToPortal(simpleCorridorRoom, portalSimpleCorGallery);
		
		portalSimpleCorCanada->setToPortal(canadaRoom, portalCanada);
		portalCanada->setToPortal(simpleCorridorRoom, portalSimpleCorCanada);
		
		portalSimpleCorPendulum->setToPortal(pendulumRoom, portalPendulum);
		portalPendulum->setToPortal(simpleCorridorRoom, portalSimpleCorPendulum);
		
		setCurrentRoom(portalRoom);
	}
	
	Room* getCurrentRoom() {
		return currentRoom;
	}
	
	
	void setCurrentRoom(Room* room){
		currentRoom = room;
	}
	
};
